import Call from "./Call";

export const requestUserLogin = async (username: string, password: string, mantraKey: string) => {
	const options = {
		url: 'http://192.168.1.148:8081/oauth/token',
		method: 'POST',
		auth: {
			username: 'mantra',
			password: 'mantraERP12252***',
		},
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		data: {
			grant_type: 'password',
			username,
			password,
			mantraKey
		}
	};
	const response = await Call(options);
	// @ts-ignore
	if (response.status === 200)
		// @ts-ignore
		window.localStorage.setItem('access_token', response.data.access_token)
};