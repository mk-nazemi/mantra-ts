import axios from 'axios';

function Call(options: object) {
	let response;
	let isLoading;
	let isError;
		if (isLoading) return;
		const fetchData = async () => {
			isError = false;
			isLoading = true;
			try {
				const result = await axios(options);
				response = result.data;
			} catch (error) {
				isError = true;
			}
			isLoading = false;
		};
		fetchData();
	return { response, isLoading, isError };
}

export default Call;
