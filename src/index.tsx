// import "airbnb-js-shims";
//
import React from "react";
import ReactDOM from "react-dom";

// store and persist store.
import { Router } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";

import { ConnectedRouter } from "connected-react-router";
import configureStore, { history } from "./redux/index";

// comps, utils
// import { store, persistor } from "./redux";
import "./i18n";
// import { history } from "./redux/index";
import App from "./App";

// import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
//

import * as serviceWorker from "./serviceWorker";

const store = configureStore(/* provide initial state if any */);

//
// {/* <Router history={history}> */}
// {/* <PersistGate persistor={persistor}> */}
// {/* </PersistGate> */}
// {/* </Router> */}
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App location={history.location} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);

// ----------------------------------------------------------------------
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
