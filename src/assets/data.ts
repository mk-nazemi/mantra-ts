
export const data = [
  {
    id: 0,
    name: 'banana',
    price: 123,
    date: '12/19/2018',
  },
  {
    id: 1,
    name: 'spinach',
    price: 282,
    date: '27/05/2019',
  },
  {
    id: 2,
    name: 'icecream',
    price: 2544,
    date: '10/10/2006',
  },
  {
    id: 3,
    name: 'bagel',
    price: 1236,
    date: '06/02/2017',
  },
  {
    id: 4,
    name: 'chips',
    price: 1623,
    date: '04/15/2007',
  },
  {
    id: 5,
    name: 'water',
    price: 8523,
    date: '04/15/2008',
  },
  {
    id: 6,
    name: 'ab',
    price: 612,
    date: '04/15/2009',
  },
  {
    id: 7,
    name: 'rice',
    price: 849,
    date: '04/15/2019',
  },
  {
    id: 8,
    name: 'chicken',
    price: 7216,
    date: '04/15/2018',
  },
  {
    id: 9,
    name: 'chocolate',
    price: 984,
    date: '04/15/2017',
  },
  {
    id: 10,
    name: 'nuggets',
    price: 11235,
    date: '04/15/2016',
  },
  {
    id: 11,
    name: 'flour',
    price: 5643,
    date: '04/15/2015',
  },
  {
    id: 12,
    name: 'bread',
    price: 2374,
    date: '04/15/2014',
  },
  {
    id: 13,
    name: 'cream',
    price: 4678,
    date: '04/15/2013',
  },
  {
    id: 14,
    name: 'sushi',
    price: 14567,
    date: '04/15/2012',
  },
];
