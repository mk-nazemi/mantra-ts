// styles
import "./LoginPage.scss";

// packages
import React from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import {useSpring, animated} from 'react-spring';
// reuse components
import { Card } from "../../components/Card/Card";
import { Login } from "../../components/Login/Login";

function _LoginPage() {

  const animation = useSpring({
    transition: '0.3s',
    opacity: 1,
    from: {opacity: 0},
  });

  return (
    <div className="login-page">
      <animated.div
        style={animation}
      >
        <Login />
      </animated.div>
    </div>
  );
}

export const LoginPage = _LoginPage;
