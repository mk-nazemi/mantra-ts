// styles
import styles from "./MainPageStyles";

// packages
import React, { useState } from "react";
import { withStyles } from "@material-ui/styles";
import classNames from "classnames";
// re-use components
import { RawTab } from "../../components/layout/RawTab/RawTab";
import ChevronLeft from "../../components/icons/ChevronLeftIcon/ChevronLeftIcon";
import { MainCard } from "../../components/MainCard/MainCard";
import { SideBar } from "../../components/layout/SideBar/SideBar";
import { QuickAccessRow } from "../../components/layout/QuickAccessRow/QuickAccessRow";
import { Introduction } from "../../components/layout/Introduction/Introduction";
import { LogOutButton } from "../../components/BUTTONS/LogOutButton/LogOutButton";
// import { SingleDatePicker } from "../../components/SingleDatePicker/SingleDatePicker";
// import { DateRangePicker } from "../../components/DateRangePicker/DateRangePicker";
// import { DropDown } from "../../components/BUTTONS/DropDown/DropDown";
// import TreeHierarchy from "../../components/TreeHierarchy/TreeHierarchy";
//import { CrewForm } from "../../components/FormsUI/CrewForm/CrewForm";
// import { RoleForm } from "../../components/FormsUI/RoleForm/RoleForm";
import { SwitchLanguageButton } from "../../components/SwitchLanguageButton/SwitchLanguageButton";
import {OrganizationChartForm} from "../../components/FormsUI/OrganizationChartForm/OrgranizationChartForm";



//
interface AppProps {
  // injected style props
  classes: {
    root: string;
    sideBarContainer: string;
    sideBarClose: string;
    mainContent: string;
    header: string;
    chevronContainer: string;
    chevron: string;
    accessRow: string;
    options: string;
    footer: string;
  };
}

interface QuickAccessChip {
  key: number;
  label: string;
}

function _MainPage(props: AppProps) {
  const [isSideBarOpen, setIsSideBarOpen] = useState(false);
  const [currentDate, setCurrentDate] = useState(true);
  const [quickAccessItems, setQuickAccessItems] = useState([
    { key: 0, label: "پرسنل" }
  ]);
  const [rawTabItems, setRawTabItems] = useState([
    "پرسنل",
    "انبار",
    "حمل",
    "مالیات",
    "انتشارات",
    "بازار",
    "مدیریت"
  ]);
  // const [tableData, setTableData] = useState("");
  const { classes } = props;

  const handleDrawerOpen = () => {
    setIsSideBarOpen(true);
  };

  const handleAddToQuickAccess = (item: string) => {
    let newQuickAccessItem = item;
    if (quickAccessItems.length >= 7) {
      return;
    }
    if (window.innerWidth < 900 && quickAccessItems.length >= 6) {
      return;
    }
    quickAccessItems.map(chip => {
      if (item === chip.label) {
        newQuickAccessItem = "";
      }
    });
    if (newQuickAccessItem.length < 1) {
      return;
    }
    const newQuickAccessItems = [
      ...quickAccessItems,
      { key: quickAccessItems.length + 1, label: item }
    ];
    setQuickAccessItems(newQuickAccessItems);
  };

  const handleDeleteQuickAccessChip = (chipToDelete: QuickAccessChip) => {
    setQuickAccessItems(chips =>
      chips.filter(chip => chip.key !== chipToDelete.key)
    );
  };

  return (
    <div className={classes.root}>
      {isSideBarOpen && (
        <div className={classes.sideBarContainer}>
          <SideBar
            addToQuickAccess={(item: string) => handleAddToQuickAccess(item)}
            setIsOpen={(status: boolean) => setIsSideBarOpen(status)}
            isOpen={isSideBarOpen}
          />
        </div>
      )}
      <div
        className={classNames(classes.mainContent, {
          [classes.sideBarClose]: !isSideBarOpen
        })}
      >
        <div className={classes.header}>
          <div className={classes.accessRow}>
            {!isSideBarOpen && (
              <div
                role="presentation"
                onClick={handleDrawerOpen}
                className={classes.chevronContainer}
              >
                <ChevronLeft className={classes.chevron} />
              </div>
            )}

            <QuickAccessRow
              items={quickAccessItems}
              isSideBarOpen={isSideBarOpen}
              deleteChip={(chip: QuickAccessChip) =>
                handleDeleteQuickAccessChip(chip)
              }
            />
            <div className={classes.options}>            
              <Introduction />
              <LogOutButton />
              <SwitchLanguageButton />
            </div>
          </div>
        </div>
        <RawTab
          items={rawTabItems}
          passActiveTab={(item: string) => console.log(item)}
        />
        <MainCard isFullSize={isSideBarOpen}>
          <div
            style={{
              height: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {/* {tableData && <p>hello dear {tableData}</p>} */}
            {/* <TableMain />*/}
            {/* <DropDown /> */}
            {/* <TreeHierarchy /> */}
            {/* <DropDown /> */}
            {/*<TreeHierarchy />*/}
            {/* <CrewForm /> */}
            <OrganizationChartForm />
          </div>
        </MainCard>
        <div className={classes.footer}>
          {/*<SingleDatePicker setDate={(date) => setCurrentDate(date)} />*/}
          {/*<DateRangePicker />*/}
        </div>
      </div>
    </div>
  );
}

export const MainPage = withStyles(styles)(_MainPage);
