import { createStyles, Theme } from "@material-ui/core/styles";
import { colors, newColor } from "../../styled/Variables";
import sizes from "../../styled/sizes";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      height: "100vh",
      display: "flex",
      flexDirection: "row",
      direction: "rtl",
      backgroundColor: `${newColor.darkest}`
    },
    sideBarContainer: {
      width: "15%",
      position: "relative",
      marginBottom: "1rem",
      [sizes.down("sm")]: {
        width: "30%",
        minWidth: "150px",
        position: 'absolute',
        height: '100%',
        marginBottom: '0',
        zIndex: 100,
      }
    },
    sideBarClose: {
      paddingRight: "1rem",
    },
    mainContent: {
      display: "flex",
      flexDirection: "column",
      width: "100%",
      paddingLeft: "1rem",
      [sizes.down("sm")]: {
        paddingRight: '1rem',
      }
    },
    header: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      borderRadius: "0.5rem",
      width: "100%",
      margin: "0.5rem 0 0.5rem auto",
      position: "relative"
    },
    chevronContainer: {
      position: "absolute",
      cursor: "pointer",
      right: "1rem"
    },
    chevron: {
      color: colors.gray500
    },
    accessRow: {
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      borderRadius: "0.5rem",
      // backgroundColor: "rgb(250, 188, 96)",
      "& > div": {
        display: "flex"
      }
    },
    options: {
      display: "flex",
      justifyContent: "space-around",
      position: "absolute",
      left: "2rem"
    },
    footer: {
      display: "flex",
      alignItems: "center",
      borderRadius: "0.5rem",
      width: "100%",
      height: "52px",
      margin: "0.5rem 0 0.5rem auto",
      backgroundColor: `${newColor.darker}`
    }
  });

export default styles;
