import axios, { AxiosInstance } from "axios";
import { Dispatch } from "redux";
import { returnErrors } from "../error/action";

import {
  AuthInfo,
  //
  AuthActionTypes,
  //
  AuthSignInStartAction,
  AuthSignInSuccessAction,
  AuthSignInFailureAction,
  //
  AuthUserLoadingAction,
  AuthUserLoadedAction,
  //
  AuthFetchUserAction,
  AuthSetCurrentUserAction,
  //
  AuthErrorAction,
  //
  AuthSignOutStartAction,
  AuthSignOutSuccessAction,
  AuthSignOutFailureAction,
  //
  AuthAction
} from "./types.enum";

// export const fetchTodos = () => {
//   return async (dispatch: Dispatch) => {
//     const response = await axios.get<AuthInfo[]>(url);

//     dispatch<FetchTodosAction>({
//       type: AuthActionTypes.fetchTodos,
//       payload: response.data
//     });
//   };
// };

// export const deleteTodo = (id: number): DeleteTodoAction => {
//   return {
//     type: AuthActionTypes.deleteTodo,
//     payload: id
//   };
// };

//----------------------------------------------------------------------
// Setup config/headers and token
export const getTokenConfig = (getState: any) => {
  // Get token from local storage
  const token = getState().userAuth.token;

  // Headers
  const config = {
    headers: {
      "Content-type": "application/json",
      Authorization: ""
    }
  };

  // If token, add to headers
  if (token) {
    // x-auth-token
    config.headers["Authorization"] = token;
  }

  return config;
};

//--------------------------------------------------------------------------------
// Check token & load user

export const loadUser = () => (dispatch: Dispatch, getState: any) => {
  // User loading fetchUser
  dispatch<AuthUserLoadingAction>({ type: AuthActionTypes.UserLoading });

  axios.get("/api/auth/user", getTokenConfig(getState)).then(res =>
    dispatch<AuthUserLoadedAction>({
      type: AuthActionTypes.UserLoaded,
      payload: res.data
    })
  );
  // .catch(err => {
  //   dispatch(returnErrors(err.response.data, err.response.status));
  //   dispatch<AuthErrorAction>({
  //     type: AuthActionTypes.AuthError
  //   });
  // });
};

//-------------------------------------------------------------------------------
// Login User

export const LoginUser = (formValues: AuthInfo) => (dispatch: Dispatch) => {
  // Headers
  const { username, password, mk } = formValues;
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  // Request body
  const body = JSON.stringify({ username, password, mk });

  axios
    .post("/api/login", body, config)
    .then(res =>
      dispatch<AuthSignInSuccessAction>({
        type: AuthActionTypes.SignInSuccess,
        payload: res.data
      })
    )
    .catch(err => {
      // dispatch<AuthSignInFailureAction>(
      //   returnErrors(
      //     err.response.data,
      //     err.response.status,
      //     AuthActionTypes.SignInFailure
      //   )
      // );
      dispatch<AuthSignInFailureAction>({
        type: AuthActionTypes.SignInFailure
      });
    });
};

//--------------------------------------------------------------

// Logout User
export const signOut = (): AuthAction => {
  return {
    type: AuthActionTypes.SignOutSuccess
  };
};

//--------------------------------------------------------
