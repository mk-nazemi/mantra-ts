import {
  AuthAction,
  AuthState,
  AuthActionTypes,
  //
  AuthSignInStartAction,
  AuthSignInSuccessAction,
  AuthSignInFailureAction,
  //
  AuthUserLoadingAction,
  AuthUserLoadedAction,
  //
  AuthFetchUserAction,
  AuthSetCurrentUserAction,
  //
  AuthErrorAction,
  //
  AuthSignOutStartAction,
  AuthSignOutSuccessAction,
  AuthSignOutFailureAction
} from "./types.enum";
//

const authInitialState: AuthState = {
  token: localStorage.getItem("token") || "",
  isAuthenticated: false,
  isLoading: false
  // user: null
};
export const authReducer = (
  state: AuthState = authInitialState,
  action: AuthAction
) => {
  switch (action.type) {
    case AuthActionTypes.UserLoading:
      return {
        ...state,
        isLoading: true
      };

    case AuthActionTypes.UserLoaded:
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false
        // user: action.payload
      };

    case AuthActionTypes.SignInSuccess:
      localStorage.setItem("token", action.payload.token);
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        isLoading: false
      };
    // case AUTH_ERROR:
    // case LOGIN_FAIL:
    // case LOGOUT_SUCCESS:
    // case REGISTER_FAIL:
    case AuthActionTypes.AuthError:
    case AuthActionTypes.SignInFailure:
      localStorage.removeItem("token");
      return {
        ...state,
        token: null,
        // user: null,
        isAuthenticated: false,
        isLoading: false
      };

    default:
      return state;
  }
};
