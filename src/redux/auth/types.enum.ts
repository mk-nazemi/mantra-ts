// import { FetchTodosAction, DeleteTodoAction } from "./action";

// ----------- USER INPUT FOR LOGIN ----------------
export interface AuthInfo {
  mk: string;
  username: string;
  password: boolean;
}

export interface AuthPayload {
  token: string;
}

// ------------ STATE OF USER ------------------
export interface AuthState {
  token: string;
  isAuthenticated: boolean;
  isLoading: boolean;
}

// ---------------------------------------------
export enum AuthActionTypes {
  SignInFailure,
  SignInStart,
  SignInSuccess,
  //
  UserLoading,
  UserLoaded,
  //
  FetchUser,
  SetCurrentUser,
  //
  AuthError,
  //
  SignOutStart,
  SignOutSuccess,
  SignOutFailure
}

// ----------- SingInAction ----------
export interface AuthSignInStartAction {
  type: AuthActionTypes.SignInStart;
}
export interface AuthSignInSuccessAction {
  type: AuthActionTypes.SignInSuccess;
  payload: AuthPayload;
}
export interface AuthSignInFailureAction {
  type: AuthActionTypes.SignInFailure;
  // payload: AuthInfo[];
  // payload: AuthPayload;
}
// ------------------------------------

// ------------ Loading states --------
export interface AuthUserLoadingAction {
  type: AuthActionTypes.UserLoading;
}
export interface AuthUserLoadedAction {
  type: AuthActionTypes.UserLoaded;
  payload: AuthPayload;
}

// -------- Fetch and SetUser ---------
export interface AuthFetchUserAction {
  type: AuthActionTypes.FetchUser;
  // payload: AuthInfo[];
  payload: AuthPayload;
}
export interface AuthSetCurrentUserAction {
  type: AuthActionTypes.SetCurrentUser;
  // payload: AuthInfo[];
  payload: AuthPayload;
}
// ------------------------------------

// ------------- AuthError -------------
export interface AuthErrorAction {
  type: AuthActionTypes.AuthError;
}
// ------------------------------------

// ----------- SignOutAction ----------
export interface AuthSignOutStartAction {
  type: AuthActionTypes.SignOutStart;
  // payload: AuthInfo;
}
export interface AuthSignOutSuccessAction {
  type: AuthActionTypes.SignOutSuccess;
  // payload: AuthInfo[];
}
export interface AuthSignOutFailureAction {
  type: AuthActionTypes.SignOutFailure;
  // payload: AuthInfo[];
}
// ------------------------------------

export type AuthAction =
  | AuthSignInStartAction
  | AuthSignInSuccessAction
  | AuthSignInFailureAction
  | AuthUserLoadingAction
  | AuthUserLoadedAction
  | AuthFetchUserAction
  | AuthSetCurrentUserAction
  | AuthErrorAction
  | AuthSignOutStartAction
  | AuthSignOutSuccessAction
  | AuthSignOutFailureAction;
