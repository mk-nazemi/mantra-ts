export enum ErrorActionTypes {
  GetError,
  ClearError
}

export interface GetErrorInfo {
  msg?: string;
  status?: number;
  id?: number | null;
}

export interface ErrorGetErrorAction {
  type: ErrorActionTypes.GetError;
  payload: GetErrorInfo;
}
export interface ErrorClearErrorAction {
  type: ErrorActionTypes.ClearError;
}

export type ErrorAction = ErrorGetErrorAction | ErrorClearErrorAction;
