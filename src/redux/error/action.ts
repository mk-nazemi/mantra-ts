import { ErrorActionTypes, GetErrorInfo } from "./types.enum";

// RETURN ERRORS
export const returnErrors = (errorInfo: GetErrorInfo) => {
  const { msg, status, id = null } = errorInfo;
  return {
    type: ErrorActionTypes.GetError,
    payload: { msg, status, id }
  };
};

// CLEAR ERRORS
export const clearErrors = () => {
  return {
    type: ErrorActionTypes.ClearError
  };
};
