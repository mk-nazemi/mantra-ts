// can Import middleware with compose
import { createStore, applyMiddleware, Store, compose } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
//
import createRootReducer from "./rootReducer";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";
//
// import { persistStore } from "redux-persist";
// import rootReducer from "./rootReducer";
// import { createLogger } from "redux-logger";
// -----------------------------------------------------------------------

export const history = createBrowserHistory();

const initialState = {};

let store: Store;
export default function configureStore(preloadedState = initialState) {
  if (process.env.NODE_ENV === "development") {
    store = createStore(
      // root reducer with router state
      createRootReducer(history),
      preloadedState,
      composeWithDevTools(
        applyMiddleware(
          // for dispatching history actions
          routerMiddleware(history),
          // ... other middlewares ...
          thunk
        )
      )
    );
  } else {
    store = createStore(
      // root reducer with router state
      createRootReducer(history),
      preloadedState,
      compose(
        applyMiddleware(
          // for dispatching history actions
          routerMiddleware(history),
          // ... other middlewares ...,
          thunk
        )
      )
    );
  }

  return store;
}

// const logger: Middleware = createLogger();
// const middlewares = [thunk];

// const composeEnhancers =
//   (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//

// store = createStore(
//   rootReducer,
//   initialState,
//   // applyMiddleware(thunk, logger)
//   // composeEnhancers(applyMiddleware(thunk))
//   composeWithDevTools(applyMiddleware(thunk))
// );

// const persistor = persistStore(store);

// export { store, persistor };

// declare global {
//   interface Window {
//     browserHistory: History;
//   }
// }

// // basename: basePath
// export const history = createBrowserHistory();

// window.browserHistory = history;
