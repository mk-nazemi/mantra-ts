import { combineReducers, Action } from "redux";
import { connectRouter } from "connected-react-router";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
// ===========================================================================
import { reducer as formReducer } from "redux-form";
import { authReducer } from "./auth";
import { History } from "history";
import { AuthState } from "./auth/types.enum";
// ===========================================================================
const persistConfig = {
  key: "root",
  storage
  // whitelist: ['cart']
};

export interface StoreState {}

const createRootReducer = (history: History<History.PoorMansUnknown>) =>
  combineReducers<StoreState>({
    router: connectRouter(history),
    // form: formReducer,
    auth: authReducer
  });
// const rootReducer = combineReducers<StoreState>({
//   form: formReducer,
//   auth: authReducer
//   // cart: cartReducer,
//   // directory: directoryReducer,
//   // shop: shopReducer
//   // user: () => 1
// });

// const rootReducer = persistReducer(persistConfig, createRootReducer);

export default createRootReducer;
