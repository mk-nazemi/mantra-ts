// eval $(ssh-agent)
// ssh-add ~/.ssh/id_rsa
// ssh -T git@gitlab.com
// ietf language
// ----------------------------------------------
// style
import "./styled/TransitionLogin.css";
import "./styled/scss/base.scss";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styled/App";
// packages
import React, { Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import { useSpring, animated } from "react-spring";
import i18next from "i18next";
// re use components
import { ErrorBoundary } from "./components/Loading/ErrorBoundary/ErrorBoundary";
import { Spinner } from "./components/Loading/Spinner/Spinner";
import { LoginPage } from "./pages/LoginPage/LoginPage";
import { MainPage } from "./pages/MainPage/MainPage";
import { Location } from "history";

interface AppProps {
  location: Location;
  classes: {
    appDiv: string;
  };
}

function _App(props: AppProps) {
  // componentDidMount() {
  //   if (this.props.token) {
  //     history.push('/userland');
  //   }
  // const { t } = useTranslation();

  // function handleClick(lang) {
  // i18next.changeLanguage(lang);
  // <button onClick={() => this.handleClick("ko")}
  // }
  // }
  const animation = useSpring({
    transition: "0.3s",
    opacity: 1,
    from: { opacity: 0 }
  });

  const { classes } = props;
  return (
    <div className={classes.appDiv}>
      <animated.div style={animation} key={props.location.pathname}>
        <Switch location={props.location}>
          <ErrorBoundary>
            <Suspense fallback={<Spinner />}>
              <Route exact path='/login' component={LoginPage} />
              <Route exact path='/' component={MainPage} />
            </Suspense>
          </ErrorBoundary>
        </Switch>
      </animated.div>
    </div>
  );
}

export default withStyles(styles)(_App);
