export const pads = {
  sm: "0.5rem",
  md: "1rem",
  lg: "1.5rem",
  smLR: "0 0.5rem",
  mdLR: "0 1rem",
  lgLR: "0 1.5rem",
  smTB: "0.5rem 0",
  mdTB: "1rem 0",
  lgTB: "1.5rem 0"
};

export const newColor = {
  bg: "#f3f3f3",
  pars: "#12cad6",
  sarBarg: "#dff0ea",
  lighter: "#c0ffb3",
  light: "#52de97",
  semiDark: "#12d3cf",
  darker: "#3c9d9b",
  darkest: "#394a6d"
};
export const margins = {
  sm: "0.5rem",
  md: "1rem",
  lg: "1.5rem",
  smLR: "0 0.5rem",
  mdLR: "0 1rem",
  lgLR: "0 1.5rem",
  smTB: "0.5rem 0",
  mdTB: "1rem 0",
  lgTB: "1.5rem 0",
  autoLR: "0 auto",
  autoTB: "auto 0"
};
export const colors = {
  gray900: "#fafafa",
  gray700: "#ddd",
  gray500: "#636363",
  gray300: "#21252B",
  brightest: "#fff",
  darkest: "#000"
};
export const radius = {
  sm: "0.25rem",
  md: "0.5rem",
  circle: "50%"
};
export const border = {
  df: "1px solid #ddd"
};
export const zIndex = {
  normal: "1",
  Maximum: "999999"
};
