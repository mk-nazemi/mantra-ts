interface sizePattern {
  xs?: string;
  sm?: string;
  md?: string;
  lg?: string;
  xl?: string;
}

export default {
  up(size: keyof sizePattern) {
    const sizes: sizePattern = {
      xs: "575.98px",
      sm: "767.98px",
      md: "991.98px",
      lg: "1199.98px",
      xl: "1600px"
    };
    return `@media (min-width: ${sizes[size]})`;
  },
  down(size: keyof sizePattern) {
    const sizes: sizePattern = {
      xs: "575.98px",
      sm: "767.98px",
      md: "991.98px",
      lg: "1199.98px",
      xl: "1600px"
    };
    return `@media (max-width: ${sizes[size]})`;
  }
};
