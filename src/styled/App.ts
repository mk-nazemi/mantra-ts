import { Theme, createStyles } from "@material-ui/core/styles";

const styles = (theme: Theme) =>
  createStyles({
    appDiv: {
      fontFamily: 'Ganjnameh',
      maxHeight: '100vh',
      minHeight: '100vh',
      height: '100vh',
      maxWidth: '100vw',
      width: '100vw',
      fontSize: '1rem',
    },
  });

export default styles;
