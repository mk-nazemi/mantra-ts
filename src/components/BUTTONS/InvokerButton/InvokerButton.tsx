import React from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./InvokerButtonStyles";
import { Introduction } from "../../layout/Introduction/Introduction";
import Setting from "../../icons/SettingIcon/SettingIcon";
import { MuiModal } from "../../MuiModal/Modal";
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";

interface AppProps {
  classes: {
    root: string;
    settingIcon: string;
    paper: string;
    childrenContainer: string;
    children: string;
    section: string;
  };
}
const _InvokerButton: React.FC<AppProps> = props => {
  const { classes } = props;
  const [open, setOpen] = React.useState(false);

  const handleModalOpen = () => {
    setOpen(true);
  };

  return (
    <div className={classes.root}>
      <div
        className={classes.settingIcon}
        role="presentation"
        onClick={handleModalOpen}
      >
        فرم جستجو پست سازمانی
      </div>
      <MuiModal
        isOpen={open}
        setIsOpen={(state: boolean) => {
          setOpen(state);
        }}
      >
        <Fade in={open}>
          <Paper>
            <div className={classes.childrenContainer}>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
            </div>
          </Paper>
        </Fade>
      </MuiModal>
    </div>
  );
};

export const InvokerButton = withStyles(styles)(_InvokerButton);
