import { newColor } from "./../../../styled/Variables";
import { Theme, createStyles } from "@material-ui/core/styles";
import { radius } from "../../../styled/Variables";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "center",
      marginLeft: "1rem"
    },
    settingIcon: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "25rem",
      margin: "1rem auto",
      height: "4rem",
      backgroundColor: `${newColor.darkest}`,
      fontSize: "1.6rem",
      color: "white",
      transition: "all 0.5s ease-in",
      cursor: "pointer",
      borderRadius: "2px",
      "-webkit-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
      "-moz-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
      "box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
      "&:hover": {
        backgroundColor: "#718FC8"
      }
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    paper: {
      backgroundColor: "white",
      border: "2px solid #000"
    },
    help: {
      color: "white"
    },
    childrenContainer: {
      width: "100%",
      display: "flex",
      alignItems: "center",
      flexWrap: "wrap",
      justifyContent: "space-evenly",
      margin: "1rem auto"
    },
    children: {
      flex: "1fr 1fr 1fr",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      margin: "0.5rem auto",
      width: "30%"
    },
    section: {
      width: "60px",
      height: "60px",
      backgroundColor: "#ffab91"
    }
  });

export default styles;
