import { createStyles, Theme } from "@material-ui/core/styles";
import { radius } from "../../../styled/Variables";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: "3.5rem",
      height: "3.5rem",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#ff5959",
      borderRadius: radius.circle,
      cursor:"pointer",
      position:"absolute",
      left:"-1.2rem",
      marginTop:"0.5rem"
    },
    extendedIcon: {
      marginRight: theme.spacing(1)
    }
  });

export default styles;
