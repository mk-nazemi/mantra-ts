import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./LogOutButtonStyles";
import LogOut from "../../icons/LogOutIcon/LogOutIcon";

interface AppProps {
  classes: {
    root: string;
  };
}

function _LogOut(props: AppProps) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <LogOut />
    </div>
  );
}

export const LogOutButton = withStyles(styles)(_LogOut);
