import { createStyles } from "@material-ui/core/styles";

const styles = () =>
  createStyles({
    container: {
      width: "12rem",
      maxHeight: "4rem",
      cursor: "pointer",
      height: "4rem",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      backgroundColor: "#FCE8F1",
      borderRadius: "3px",
      transition: ".3s all ease",
      "-webkit-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.4)",
      "-moz-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.4)",
      "box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.4)",
      "&:hover": {
        backgroundColor: "yellow",
        transform: "scale(1.01)"
      },
      "&:active": {
        backgroundColor: "yellow",
        transform: "scale(0.98)"
      }
    },
    text: {
      fontSize: "1.4rem",
      color: "#000",
      paddingRight: "1rem"
    },
    icon: {
      width: "3rem",
      height: "3rem",
      marginLeft: "0.5rem",
      marginTop: "0.8rem"
    }
  });

export default styles;
