import React from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./ProceedButtonStyles";

interface AppProps {
  iconName?: string;
  btnProps: {
    backColor: string;
    textColor: string;
    textOf: string;
    alt?: string;
  };
  classes: {
    container: string;
    text: string;
    icon: string;
  };
}

function _ProceedButton(props: AppProps) {
  const {
    classes,
    iconName,
    btnProps: { backColor, textOf, alt, textColor }
  } = props;
  return (
    <div className={classes.container} style={{ backgroundColor: backColor }}>
      <p className={classes.text} style={{ color: textColor }}>
        {textOf}
      </p>
      <div className={classes.icon}>
        <img
          src={iconName}
          alt={alt}
          style={{ width: "2rem", height: "2rem" }}
        />
      </div>
    </div>
  );
}

export const ProceedButton = withStyles(styles)(_ProceedButton);
