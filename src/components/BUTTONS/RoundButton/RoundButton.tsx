// style

// packages
import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
// re use components

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      borderRadius: "50%",
      fontSize: "1.1rem",
      color: "white",
      width: "3.25rem",
      height: "3.25rem",
      margin: "1rem",
      letterSpacing: "1px",
      backgroundColor: "#0288d1",
      border: "none",
      outline: "none",
      cursor: "pointer",
      transition: "all 0.4s ease",
      "&:hover": {
        backgroundColor: "#01579b"
      },
      "&:active": {
        backgroundColor: "#01579b",
        transform: "scale(0.98)"
      },
      "-webkit-box-shadow": "0px 4px 4px 1px rgba(122, 121, 122, 0.16)",
      "-moz-box-shadow": "0px 4px 4px 1px rgba(122, 121, 122, 0.16)",
      "box-shadow": "0px 4px 4px 1px rgba(122, 121, 122, 0.16)"
    }
  })
);

interface AppProps {
  children?: JSX.Element | JSX.Element[] | string;
  type: string;
  handleOpen: () => void;
}

const _RoundButton = (props: AppProps) => {
  const classes = useStyles();

  return (
    <button className={classes.root} type="button" onClick={props.handleOpen}>
      {props.children}
    </button>
  );
};

export const RoundButton = _RoundButton;
