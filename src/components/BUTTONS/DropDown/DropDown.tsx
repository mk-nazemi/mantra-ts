import React from 'react';
import { withStyles } from "@material-ui/styles";
import styles from './DropDownStyles';
import ArrowDropDown from '../../icons/ArrowDropDown/ArrowDropDown';

interface AppProps {
  classes: {
    root: string;
    container: string;
    tab: string;
    tabs: string;
    link: string;
    //subHeader:string;
  }
}
//const data = [{Header:'Fruits', items:['Link 1', 'Link 2', 'Link 3', 'Link 4','Link 5','Link 6']},{Header:'Vegetables', items:['Link 7', 'Link 8', 'Link 9', 'Link 10','Link 11','Link 12']} ];
const data = [{Header:'Fruits', items:['حسابداری', 'حسابداری', 'حسابداری']} ];

function _DropDown(props: AppProps) {
  const [tabs, setTabs] = React.useState(data);
  const [inputData, setInputData] = React.useState("");
  const { classes } = props;
  const searchInTabs = (value: string) =>{
   setInputData(value)
   
  }
  
   
  return (
    <div className={classes.root}>
      <nav>
        <div className={classes.container}>
          
          <p className={classes.tab}> 
              حسابداری
              <ArrowDropDown/>
          </p>
          {/* <input placeholder="search" value={inputData} onChange={(e) => searchInTabs(e.target.value)} /> */}
          <div className={classes.tabs}>
          {
        data.map(tab => {
          return(<div>
            {/* <h3 className={classes.subHeader}>{tab.Header}</h3> */}
            {tab.items.map(item => (
             <p className={classes.link}>{item}</p>
             ))}
          </div>)
        })
      }
            
          </div>
        </div>
      </nav>
    </div>
    );
}
export const DropDown = withStyles(styles)(_DropDown);