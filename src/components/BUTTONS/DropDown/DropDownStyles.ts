import { createStyles } from "@material-ui/core/styles";
import { relative } from "path";

const styles = () => createStyles({
    root: {
        width: "98%",
        boxShadow: "none",
        backgroundColor: "inherit",
        margin: "0 auto",
        padding: "0",
        boxSizing: "border-box",

    },
    container: {
        float: "right",
        width: "17rem",
        height: "100%",
        position: "relative",
        backgroundColor: "#fff",
        borderRadius:'4px',
        border:"2px solid #79589f",
        boxShadow: " 0 3px 3px rgba(0, 0, 0, 0.16)",
        cursor: "pointer",
        '&:hover > p': {
            cursor: "pointer",
            backgroundColor: "#79589f",
            color:"#fff"

        },
        '&:hover > div': {
            display: "block"

        }
    },
    tab: {
        textAlign: "center",
        width: "100%",
        height: "100%",
        display: "block",
        lineHeight: "50px",
        transition: "background-color 0.2s ease",
        textTransform: "uppercase",
        position:"relative"

    },

    tabs: {
        display: "none",
        overflow: "hidden",
        backgroundColor: "white",
        minWidth: "100%",
        position: "absolute",
        boxShadow: "0 3px 3px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23)",
        padding: "0 1px 0 1px",
        border:"2px solid #ccc",
        borderRadius:"1px",
        top:"51px"
    },
    // subHeader: {
    //     display: "block",
    //     float: "left",
    //     padding: "8px 10px",
    //     width:"100%",
    //     //margin: "2%",
    //     margin:"5px 0",
    //     textAlign: "center",
    //     backgroundColor: "#79589f",
    //     color: "#fbfbfb",
    //     borderRadius: "2px",
    //     transition: "background-color 0.2s ease",
    //     '&:hover': {
    //         cursor: "pointer",
    //         backgroundColor: "#5a3e7b"

    //     },
        

    // },
    link: {
        display: "block",
        float: "left",
        padding: "13px 0",
        width:"100%",  
        borderRadius: "2px",
        transition: "background-color 0.2s ease",
        '&:hover': {
            cursor: "pointer",
            backgroundColor: "#f8f8f8"

        },
    }
});
export default styles;

