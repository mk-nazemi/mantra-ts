import { Theme, createStyles } from "@material-ui/core/styles";
import { radius } from "../../../styled/Variables";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "center",
      marginLeft: "1rem"
    },
    settingIcon: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "10rem",
      margin: "1rem auto",
      height: "4rem",
      backgroundColor: "#718FC8",
      fontSize: "1rem",
      color: "white",
      transition: "all 0.5s ease-in",
      cursor: "pointer",
      borderRadius: "2px",
      "-webkit-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
      "-moz-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
      "box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
      "&:hover": {
        backgroundColor: "#334A7B"
      }
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    help: {
      color: "white"
    },
  });

export default styles;
