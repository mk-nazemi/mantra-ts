import React , { useState, Dispatch, SetStateAction } from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./AccessButtonStyle";
import { Introduction } from "../../layout/Introduction/Introduction";
import Setting from "../../icons/SettingIcon/SettingIcon";


interface AppProps {
  clickable: Dispatch<SetStateAction<boolean>>;
  children?: JSX.Element | JSX.Element[] | string;
  ActiveDisplay?: boolean;
  ActiveLicense?:boolean;
  classes: {
    root: string;
    settingIcon: string;    
  };
  ourLabel: string; 
}
const _AccessButton: React.FC<AppProps> = props => {
  const { classes,ourLabel } = props;
  const myFunc = () => {
    if(props.ActiveDisplay===false){
      props.clickable(!props.ActiveDisplay );
    }
       
    if(props.ActiveDisplay===true){
      alert("1")
      props.clickable(props.ActiveLicense === false);
    }
    if(props.ActiveLicense===true){
      alert("2")
      props.clickable(props.ActiveDisplay === false);
    }
    if( (props.ActiveDisplay===true) && (props.ActiveLicense===false) ){
      alert("3")
      //props.clickable(!props.ActiveDisplay);
      //props.clickable(!props.ActiveLicense);
    }

  }
  console.log(props.ActiveDisplay)
  console.log(props.ActiveLicense)
  return (
    <div onClick={myFunc} className={classes.root}>  
      <div
        className={classes.settingIcon}
        role="presentation"    
      >
        {ourLabel} 
      </div>  
     
    </div>
  );
};

export const AccessButton = withStyles(styles)(_AccessButton);
