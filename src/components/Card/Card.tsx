// style
import "./Card.scss";

// packages
import React from "react";

const _Card: React.FC = (props: { children?: React.ReactNode }) => {
  return <div className="card">{props.children}</div>;
};

export const Card = _Card;
