import React, { useState } from 'react';
import styles from "./LoginStyles";
import { withStyles } from "@material-ui/styles";
import { requestUserLogin } from '../../utils/ApiHelpers/AuthHelper';
import Person from "../icons/PersonIcon/PersonIcon";
import Lock from "../icons/LockIcon/LockIcon";
import GroupWork from "../icons/GroupWorkIcon/GroupWorkIcon";

interface AppProps {
  classes: {
    root: string;
    title: string;
    dataEntry: string;
    inputContainer: string;
    input: string;
    icon: string;
    submit: string;
    error: string;
  }
}

function _Login(props: AppProps) {
  const { classes }= props;
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [mantraKey, setMantraKey] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const validateUserData = () => {
    if (!username && !password) {
      setErrorMessage('لطفا نام کاربری و رمز عبور خود را وارد کنید.');
      return;
    }
    if (!username) {
      setErrorMessage('لطفا نام کاربری خود را وارد کنید.');
      return;
    }
    if (!password) {
      setErrorMessage('لطفا رمز عبور خود را وارد کنید.');
      return;
    }
    setErrorMessage('');
    const response = requestUserLogin(username, password, mantraKey);
    console.log(response);
    // window.location.assign('/')
  };

  return (
    <div className={classes.root}>
      <p className={classes.title}>ورود به حساب کاربری</p>
      <div className={classes.dataEntry}>
        <div className={classes.inputContainer}>
          <input
            className={classes.input}
            placeholder="نام کاربری"
            autoComplete="email"
            autoFocus
            onChange={(e) => {setErrorMessage(''); setUsername(e.target.value)}}
          />
          <div className={classes.icon}>
            <Person />
          </div>
        </div>
        <div className={classes.inputContainer}>
          <input
            className={classes.input}
            placeholder="رمز عبور"
            type="password"
            autoComplete="current-password"
            onChange={(e) => {setErrorMessage(''); setPassword(e.target.value)}}
          />
          <div className={classes.icon}>
            <Lock />
          </div>
        </div>
        <div className={classes.inputContainer}>
          <input
            className={classes.input}
            placeholder="رمز مانترا"
            type="text"
            onChange={(e) => setMantraKey(e.target.value)}
          />
          <div className={classes.icon}>
            <GroupWork />
          </div>
        </div>
      </div>
        <button
          className={classes.submit}
          type="submit"
          onClick={validateUserData}
        >
          ورود
        </button>
      <p className={classes.error}>{errorMessage}</p>
    </div>
  );
}

export const Login = withStyles(styles)(_Login);
