import { createStyles, Theme } from "@material-ui/core/styles";
import sizes from "../../styled/sizes";

const styles = (theme: Theme) =>
	createStyles({
		root: {
			direction: 'rtl',
			width: '30vw',
			height: '50vh',
			minHeight: '30rem',
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
			justifyContent: 'space-around',
			padding: '2rem',
			fontFamily: 'Ganjnameh',
			backgroundColor: '#fff',
			[sizes.down("md")]: {
				width: '60vw',
				minHeight: '25rem',
				padding: '1rem',
			},
			[sizes.down("xs")]: {
				width: '80vw',
			}
		},
		title: {
			fontSize: '1.6rem',
			fontWeight: 'bold',
			fontFamily: 'inherit',
		},
		dataEntry: {
			display: 'flex',
			flexDirection: 'column',
		},
		inputContainer: {
			position: 'relative',
		},
		input: {
			minWidth: '20rem',
			minHeight: '3.3rem',
			border: '2px solid #e8e8e8',
			borderRadius: '4px',
			outline: 'none',
			marginBottom: '2rem',
			fontFamily: 'inherit',
			padding: '0.2rem',
			'&::placeholder': {
				paddingRight: '0.5rem',
			},
			'&:last-child': {
				marginBottom: 0,
			}
		},
		icon: {
			width: '3rem',
			height: '3rem',
			position: 'absolute',
			left: 0,
			top: '0.2rem',
		},
		submit: {
			minWidth: '30rem',
			minHeight: '4rem',
			outline: 'none',
			border: 'none',
			backgroundColor: '#394a6d',
			color: '#fff',
			fontSize: '1.4rem',
			fontWeight: 'bold',
			fontFamily: 'inherit',
		},
		error: {
			minHeight: '1.5rem',
			color: 'red',
		},
	});
export default styles;
