import { createStyles } from "@material-ui/core/styles";
import sizes from "../../../styled/sizes";
import { newColor } from "../../../styled/Variables";

const styles = createStyles({
  root: {
    boxShadow: "none",
    marginRight: "2rem",
    backgroundColor: "inherit",
    padding: "1rem",
    overflow: "scroll",
    scrollbarWidth: "none",
    "-ms-overflow-style": "none",
    "&::-webkit-scrollbar": {
      display: "none"
    },
    [sizes.down("md")]: {
      maxWidth: "80%"
    }
  },
  sideBardOpen: {
    marginRight: 0
  },
  container: {
    backgroundColor: `${newColor.sarBarg}`,
    cursor: "pointer",
    alignItems: "center",
    borderRadius: "40px",
    marginRight: "1rem",
    transitionDuration: "0.2s",
    minWidth: "12rem",
    minHeight: "3rem",
    display: "flex",
    justifyContent: "space-between",
    "&:hover": {
      backgroundColor: `${newColor.lighter}`
    },
    [sizes.down("md")]: {
      marginRight: "0.5rem",
      minWidth: "8rem"
    }
  },
  chipLabel: {
    paddingRight: "0.5rem"
  },
  deleteIcon: {
    height: "1.6rem",
    marginLeft: "0.8rem"
  }
});

export default styles;
