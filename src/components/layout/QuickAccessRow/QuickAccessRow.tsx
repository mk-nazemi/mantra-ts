import React from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./QuickAccessRowStyles";
import classNames from "classnames";
import Paper from "@material-ui/core/Paper";
import HighlightOff from "../../icons/HighlightOff/HighlightOff";

interface AppProps {
  items: Array<ChipData>;
  deleteChip: Function;
  isSideBarOpen: boolean;
  classes: {
    root: string;
    sideBardOpen: string;
    container: string;
    deleteIcon: string;
    chipLabel: string;
  };
}

interface ChipData {
  key: number;
  label: string;
}

function _QuickAccessRow(props: AppProps) {
  const [chipData, setChipData] = React.useState(props.items);
  React.useEffect(() => {
    setChipData(props.items);
  }, [props.items]);
  const { classes, isSideBarOpen } = props;

  const handleDelete = (chipToDelete: ChipData) => () => {
    props.deleteChip(chipToDelete);
  };

  return (
    <Paper
      className={classNames(classes.root, {
        [classes.sideBardOpen]: isSideBarOpen
      })}
    >
      {chipData.map(data => (
        <div key={data.key} className={classes.container}>
          <p className={classes.chipLabel}>{data.label}</p>
          <div
            className={classes.deleteIcon}
            role="presentation"
            onClick={handleDelete(data)}
          >
            <HighlightOff />
          </div>
        </div>
      ))}
    </Paper>
  );
}

export const QuickAccessRow = withStyles(styles)(_QuickAccessRow);
