import { colors, margins } from "../../../styled/Variables";

import { createStyles, Theme } from "@material-ui/core/styles";
import sizes from "../../../styled/sizes";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      fontFamily: "Ganjnameh",
      margin: margins.autoLR,
      backgroundColor: '#394a6d',
      border: '1px solid #333366',
      width: "90%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      position: "relative",
      marginTop: "0.5rem",
      borderRadius: "8px",
      overflowY: "scroll",
      overflowX: "hidden",
      "&::-webkit-scrollbar": {
        display: "none"
      },
      "&::-webkit-scrollbar-track": {
        display: "none"
      },
      "&::-webkit-scrollbar-thumb": {
        background: "#737373"
      },
      [sizes.down("sm")]: {
        marginTop: 0,
        width: "100%",
        borderRadius: 0,
      }
    },
    rootClose: {
      border: "none",
      overflowY: "hidden"
    },
    chevronContainer: {
      backgroundColor: "inherit",
      padding: "1.5rem 1.5rem 1rem",
      cursor: "pointer",
      position: "sticky",
      top: 0,
      left: 0,
      display: "flex",
      flexDirection: "row-reverse",
      zIndex: 1,
      userSelect: 'none',
    },
    chevron: {
      color: colors.gray500
    },
    chevronRight: {
      right: "1rem"
    },
    list: {
      minWidth: "85%",
      padding: 0,
      margin: "0.7rem auto 0",
      position: "relative",
      right: 0,
      transition: "right 0.2s cubic-bezier(0.820, 0.085, 0.395, 0.895)"
    },
    close: {
      right: "-28rem"
    },
    expansionPanelTitle: {
      fontFamily: "inherit",
      fontSize: "1.35rem"
    },
    expansionPanelItemContainer: {
      backgroundColor: "#35495e",
      padding: "1rem 0 1rem"
    },
    expansionPanelItem: {
      fontSize: "1.2rem",
      borderRadius: "2px",
      padding: "0.5rem 0.8rem 0.5rem 0.5rem",
      width: "80%",
      margin: "0 auto",
      color: colors.brightest,
      cursor: "pointer",
      transition: "0.2s",
      "&:last-child": {
        border: "none"
      },
      "&:hover": {
        backgroundColor: "#909090",
        color: "#000"
      }
    }
  });
export default styles;
