// styles
import styles from "./SideBarStyles";
// packages
import React, { Fragment } from "react";
import { withStyles } from "@material-ui/styles";
import classNames from "classnames";
// components
import Typography from "@material-ui/core/Typography";
import ChevronRight from "../../icons/ChevronRightIcon/ChevronRightIcon";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";

const ExpansionPanel = withStyles({
  root: {
    marginBottom: "0.8rem",
    border: "none",
    background: "#404040",
    boxShadow: "none",

    "&:before": {
      display: "none"
    },
    "&$expanded": {
      margin: "auto"
    }
  },
  expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    padding: 0,
    paddingRight: "10px",
    backgroundColor: "#35495e",
    border: "none",
    minHeight: 40,
    color: "#fff",
    "&$expanded": {
      minHeight: 40
    }
  },
  content: {
    "&$expanded": {
      margin: "12px 0"
    }
  },
  expandIcon: {
    color: "#fff"
  },
  expanded: {}
})(MuiExpansionPanelSummary);

const Data = ["اطلاعات پایه", "حسابداری", "فروش", "انبار"];

const subMenu = ["پرسنل", "چارت سازمانی", "نقش"];

interface AppProps {
  addToQuickAccess: Function;
  isOpen: boolean;
  setIsOpen: Function;
  classes: {
    root: string;
    rootClose: string;
    chevronContainer: string;
    chevron: string;
    list: string;
    close: string;
    expansionPanelTitle: string;
    expansionPanelItemContainer: string;
    expansionPanelItem: string;
  };
}

const _SideBar: React.FC<AppProps> = props => {
  const { classes, isOpen, setIsOpen } = props;
  const handleDrawerClose = () => {
    setIsOpen(false);
  };

  const handleAddToQuickAccess = (item: string) => {
    props.addToQuickAccess(item);
  };

  return (
    <div className={classNames(classes.root, { [classes.rootClose]: !isOpen })}>
      <Fragment>
        {isOpen && (
          <div
            className={classes.chevronContainer}
          >
            <div role="presentation" onClick={handleDrawerClose}>
              <ChevronRight className={classes.chevron} />
            </div>
          </div>
        )}
      </Fragment>
      <ul className={classNames(classes.list, { [classes.close]: !isOpen })}>
        {Data.map(item => (
          <ExpansionPanel key={item}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.expansionPanelTitle}>
                {item}
              </Typography>
            </ExpansionPanelSummary>
            {
              //map sub menu's here
              <div className={classes.expansionPanelItemContainer}>
                {subMenu.map(item => (
                  <div
                    key={item}
                    role="presentation"
                    onDoubleClick={() => handleAddToQuickAccess(item)}
                    className={classes.expansionPanelItem}
                  >
                    {item}
                  </div>
                ))}
              </div>
            }
          </ExpansionPanel>
        ))}
      </ul>
    </div>
  );
};

export const SideBar = withStyles(styles)(_SideBar);
