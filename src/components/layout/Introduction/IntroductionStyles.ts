import { Theme, createStyles } from "@material-ui/core/styles";
import { radius, newColor } from "../../../styled/Variables";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      //display: "flex",
      alignItems: "center",
      position:"absolute",
      marginTop:"0.5rem",
      left:"2.8rem"
    },
    settingIcon: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "3.5rem",
      height: "3.5rem",
      backgroundColor: "#49beb7",
      borderRadius: radius.circle,
      cursor:"pointer"
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    paper: {
      backgroundColor: "white",
      border: "2px solid #000"
    },
    help: {
      color: "white"
    },
    childrenContainer: {
      width: "100%",
      display: "flex",
      alignItems: "center",
      flexWrap: "wrap",
      justifyContent: "space-evenly",
      margin: "1rem auto"
    },
    children: {
      flex: "1fr 1fr 1fr",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      margin: "0.5rem auto",
      width: "30%"
    },
    section: {
      width: "60px",
      height: "60px",
      backgroundColor: "#ffab91"
    }
  });

export default styles;
