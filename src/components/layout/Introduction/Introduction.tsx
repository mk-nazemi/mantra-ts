// styles
import styles from "./IntroductionStyles";
// main packages
import React from "react";
import { withStyles } from "@material-ui/styles";
// components
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";
import { MuiModal } from "../../MuiModal/Modal";
import Setting from "../../icons/SettingIcon/SettingIcon";

interface AppProps {
  classes: {
    root: string;
    settingIcon: string;
    paper: string;
    childrenContainer: string;
    children: string;
    section: string;
  };
}

function _Introduction(props: AppProps) {
  const { classes } = props;
  const [open, setOpen] = React.useState(false);

  const handleModalOpen = () => {
    setOpen(true);
  };

  return (
    <div className={classes.root}>
      <div
        className={classes.settingIcon}
        role="presentation"
        onClick={handleModalOpen}
      >
        <Setting />
      </div>
      <MuiModal
        isOpen={open}
        setIsOpen={(state: boolean) => {
          setOpen(state);
        }}
      >
        <Fade in={open}>
          <Paper>
            <div className={classes.childrenContainer}>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
              <div className={classes.children}>
                <Paper className={classes.section}>i am here</Paper>
              </div>
            </div>
          </Paper>
        </Fade>
      </MuiModal>
    </div>
  );
}

export const Introduction = withStyles(styles)(_Introduction);
