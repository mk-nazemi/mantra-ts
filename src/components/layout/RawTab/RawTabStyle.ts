import { createStyles } from "@material-ui/core/styles";
import sizes from "../../../styled/sizes";
import { newColor } from "../../../styled/Variables";

const styles = () =>
  createStyles({
    root: {
      width: "100%",
      boxShadow: "none",
      backgroundColor: "inherit",
      marginLeft: "auto"
    },
    tabs: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      alignItems: "flex-end"
    },
    Container: {
      width: "calc(90% / 8)",
      minWidth: "2rem",
      maxHeight: "2.5rem",
      marginLeft: "1rem",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-around",
      // background: "#fff1cf",
      background: `${newColor.sarBarg}`,
      borderRadius: "1.4rem 0 0 0",
      color: "black",
      cursor: "pointer",
      transitionDuration: "0.2s",
      "&:hover": {
        backgroundColor: `${newColor.lighter}`
      }
    },
    active: {
      backgroundColor: `${newColor.semiDark} !important`
    },
    tab: {
      fontFamily: "Ganjnameh",
      fontSize: "1.4rem",
      padding: "0 0.5rem",
      minWidth: 0,
      width: "79%",
      color: "black",
      [sizes.down("md")]: {
        fontSize: "1rem",
        width: "75%"
      }
    },
    deleteIcon: {
      paddingTop: "0.6rem"
    }
  });
export default styles;
