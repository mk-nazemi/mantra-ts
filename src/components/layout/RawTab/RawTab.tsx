import React from "react";
import styles from "./RawTabStyle";
import { withStyles } from "@material-ui/styles";
import classNames from "classnames";
import Paper from "@material-ui/core/Paper";
import HighlightOff from "../../icons/HighlightOff/HighlightOff";

interface AppProps {
  items: Array<string>;
  passActiveTab?: Function;
  classes: {
    root: string;
    tabs: string;
    tab: string;
    active: string;
    Container: string;
    deleteIcon: string;
  };
}

function _RawTab(props: AppProps) {
  const [tabs, setTabs] = React.useState(props.items);
  const [activeTab, setActiveTab] = React.useState("پرسنل");
  const { classes, passActiveTab } = props;

  const handleCloseTab = (tab: string) => {
    const tabIndex = tabs.indexOf(tab);
    tabs.splice(tabIndex, 1);
    setTabs([...tabs]);
  };

  return (
    <Paper className={classes.root}>
      <div className={classes.tabs}>
        {tabs.map(item => (
          <div
            role="presentation"
            key={item}
            className={classNames(classes.Container, {
              [classes.active]: activeTab === item
            })}
            onClick={() => {
              setActiveTab(item);
            }}
          >
            <div
              role="presentation"
              className={classes.tab}
              onClick={() => passActiveTab && passActiveTab(item)}
            >
              {item}
            </div>
            <div
              className={classes.deleteIcon}
              role="presentation"
              onClick={() => handleCloseTab(item)}
            >
              <HighlightOff />
            </div>
          </div>
        ))}
      </div>
    </Paper>
  );
}

export const RawTab = withStyles(styles)(_RawTab);
