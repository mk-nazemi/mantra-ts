import { Theme, createStyles } from "@material-ui/core/styles";

const styles = (theme: Theme) =>
  createStyles({
    ErrorImageOverlay: {
      height: '60vh',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    ErrorImageContainer: {
      display: 'inline-block',
      backgroundImage: 'url(https://sfds.com)',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      width: '40vh',
      height: '40vh',
    },
    ErrorImageText: {
      fontSize: '28px',
      color: '#2f8e89',
    },
  });

export default styles;
