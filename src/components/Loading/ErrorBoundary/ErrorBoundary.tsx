// styles
import styles from "./ErrorBoundaryStyles";

// packages
import React, { Component, ErrorInfo } from "react";
import { withStyles } from "@material-ui/styles";

interface AppProps {
  classes: {
    ErrorImageOverlay: string;
    ErrorImageContainer: string;
    ErrorImageText: string;
  };
}
class _ErrorBoundary extends Component<AppProps> {
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: AppProps) { // ️⚡️ does not compile in strict mode
    super(props)
  };
  state = { hasError: false };
  static getDerivedStateFromError = (error: ErrorEvent) => {
    return { hasError: true };
  };

  componentDidCatch(error: Error, info: ErrorInfo) {
    console.log(error);
  }

  render() {
    const {
      classes: { ErrorImageContainer, ErrorImageOverlay, ErrorImageText }
    } = this.props;

    if (this.state.hasError) {
      return (
        <div className={ErrorImageOverlay}>
          <div className={ErrorImageContainer} />
          <h2 className={ErrorImageText}>Sorry this page is broken.</h2>
        </div>
      );
    }
    return this.props.children;
  }
}

export const ErrorBoundary = withStyles(styles)(_ErrorBoundary);
