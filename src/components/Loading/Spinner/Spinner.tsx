// styles
import styles from "./SpinnerStyles";

// packages
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";

interface AppProps {
  classes: {
    spinnerOverlay: string;
    spinnerContainer: string;
  };
}

function _Spinner(props: AppProps) {
  const {
    classes: { spinnerContainer, spinnerOverlay }
  } = props;
  return (
    <div className={spinnerOverlay}>
      <div className={spinnerContainer} />
    </div>
  );
}

export const Spinner = withStyles(styles)(_Spinner);
