import React from "react";
import styles from "./InformationTableRoleFormStyles";
import { withStyles } from "@material-ui/styles";
import { EditAndCommit } from '../EditAndCommit/EditAndCommit';
//import { ProceedButtonDelete } from "../../BUTTONS/ProceedButtonDelete/ProceedButtonDelete";

interface AppProps {
  classes: {
    wrapper: string;
    informationTableProceedButtons: string;
  };
}

class _InformationTableRoleForm extends React.Component<AppProps> {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        <EditAndCommit />
      </div>
    );
  }
}

export const InformationTableRoleForm = withStyles(styles)(_InformationTableRoleForm);
