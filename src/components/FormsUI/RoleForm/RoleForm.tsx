import React, { useState } from "react";
import { AlwaysForm } from "../AlwaysForm/AlwaysForm";
import { InformationTableRoleForm } from "../InformationTableRoleForm/InformationTableRoleForm";
import {EditAndCommitRoleForm} from "../EditAndCommitRoleForm/EditAndCommitRoleForm";





export enum ActiveTab {
    EditAndCommit = "EditAndCommit",
    InformationTable = "InformationTable"
  }
  
  function _RoleForm() {
    const [activeTab, setActiveTab] = useState(ActiveTab.EditAndCommit);
    return (
      <AlwaysForm fn={setActiveTab}>
      {activeTab === ActiveTab.InformationTable ? (
        <InformationTableRoleForm />
      ) : (
        <EditAndCommitRoleForm />
      )}
    </AlwaysForm>
    );
  }
  
  export const RoleForm = _RoleForm;
