export const personInfoData = [
  "‫نام‬",
  "‫نام ‫خانوادگی‬",
  "‫کد‬ ‫ملی‬",
  "‫‫تاریخ‬ تولد‬",
  "‫‫شماره‬ پرسنلی‬"
];

export const accountData = [
  "‫نام‬ ‫کاربري‬",
  "‫رمز‬ ‫عبور‬",
  "‫تکرار‬ ‫رمز‬",
  "‫تاریخ‬ ‫انقضا‬"
];

export const rolesData = ["‫‫مدیر‬ فناوري‬", "‫مدیر‬ ‫‫فروشگاه‬ ها‬"];
export const cancelProps = {
  backColor: "#FCE8F1",
  textColor: "#000",
  textOf: "انصراف",
  alt: "انصراف"
};
export const deleteProps = {
  backColor: "#EC268F",
  textColor: "#fff",
  textOf: "حذف",
  alt: "حذف"
};

export const commitProps = {
  backColor: "#DEEFE1",
  textColor: "#000",
  textOf: "ثبت",
  alt: "ثبت"
};

export const newProps = {
  backColor: "#76C26D",
  textColor: "#fff",
  textOf: "ثبت",
  alt: "ثبت"
};
