import React ,{ useState } from "react";
import styles from "./EditAndCommitRoleFormStyles";
import { withStyles } from "@material-ui/styles";
import Delete from "../../icons/DeleteIcon/DeleteIcon";
import { ProceedButton } from "../../BUTTONS/ProceedButton/ProceedButton";
import cancelLogo from "../../../assets/backspace.svg";
import deleteLogo from "../../../assets/rubbish.svg";
import floppy from "../../../assets/floppy-disk.svg";
import add from "../../../assets/add.svg";
import * as yup from "yup";
import {
  personInfoData,
  accountData,
  rolesData,
  cancelProps,
  deleteProps,
  commitProps,
  newProps
} from "../EditAndCommit/dummyData";
import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray,
  withFormik,
  FormikProps
} from "formik";
import { TextField } from "@material-ui/core";
import { CustomInput } from "../CustomInput/CustomInput";
import { DisplayRole } from "../../DisplayRole/DisplayRole";

interface FormProps {
  code?: string;
  roleTitle?: string;
  
}
interface AppProps {
  classes: {
    root: string;
    form: string;
    btnContainer: string;
    roleContainer:string;
    role:string
    renderButton: string
  };
}
enum labels {
  code ="کد کاربری",
  roleTitle="عنوان کد",
  AccessMenu="دسترسی منو",
  AccessLicense="دسترسی مجوز",
  MenuHeader="منوها",
  MenuList="لیست منو انتخابی",
  LicenseHeader="مجوز ها",
  LicenseList="لیست مجوز های انتخابی"
}
function _EditAndCommitRoleForm(props:AppProps){
  const [activeDisplay, setActiveDisplay] = useState('license');
  const {classes} = props;
  const initValues: FormProps = {
    code:"",
    roleTitle:""
  };
  
  return(
    <div className={classes.root}>
      <Formik initialValues={initValues}
        // come from formik bag
        onSubmit={(data, { setSubmitting, resetForm, setErrors }) => {
          setSubmitting(true);
          // async call here
          console.log("submit form :", data);
          // async call finish
          setSubmitting(false);
          resetForm();
        }}>
        <Form className={classes.form}><div className={classes.btnContainer}>
          <ProceedButton iconName={add} btnProps={newProps} />
          <ProceedButton iconName={floppy} btnProps={commitProps} />
          <ProceedButton iconName={deleteLogo} btnProps={deleteProps} />
          <ProceedButton iconName={cancelLogo} btnProps={cancelProps} />
          </div>
          <div className={classes.roleContainer}>
            <div className={classes.role}>
            
  
              <CustomInput
                name="code"
                type="input"
                ourLabel={labels.code}
              />
              <CustomInput
                name="roleTitle"
                type="input"
                ourLabel={labels.roleTitle}
              />
              <button className={classes.renderButton} onClick={() => setActiveDisplay('menu')} >{labels.AccessMenu}</button>
              <button className={classes.renderButton} onClick={() => setActiveDisplay('license')} >{labels.AccessLicense}</button>
            </div>
          </div>
          { activeDisplay === 'license'
            ? <DisplayRole HeaderLabel={labels.LicenseHeader} ListLabel={labels.LicenseList}/>
            : <DisplayRole HeaderLabel={labels.MenuHeader} ListLabel={labels.MenuList}/>
          }
          </Form></Formik>
      
    </div>
  )
}
export const EditAndCommitRoleForm = withStyles(styles)(_EditAndCommitRoleForm);
