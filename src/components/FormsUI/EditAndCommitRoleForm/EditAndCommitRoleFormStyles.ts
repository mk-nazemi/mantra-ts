import { createStyles } from "@material-ui/core/styles";
const backColor = "#E4EBF7";
const styles = () =>
  createStyles({
    root: {
      display: "flex",
      height: "100%",
      width: "100%",
      // background: "red",
      flexDirection: "column",
      alignItems: "center"
    },
    form: {
      width: '100%',
      padding: '2rem',
      paddingTop: 0,
    },
    btnContainer: {
      display: "flex",
      justifyContent: "space-evenly",
      alignItems: "center",
      // backgroundColor: "red",
      margin: "1rem auto",
      width: "80%",
      height: "5vh"
    },
    roleContainer: {
      width: "100%"
    },
    role: {
      padding: "1rem",
      width: "100%",
      height: "60%",
      margin: "1rem auto 0rem",
      background: '#f3f3f3',
      alignItems: 'center',
      "@media (max-width: 600px)": {},
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap"
    },
    renderButton: {
      width: '10rem',
      height: "4rem",
      backgroundColor: '#394a6d',
      color: 'white',
      border: 'none',
      borderRadius: '4px',
      marginLeft: '1rem',
      marginRight: '6rem',
      fontFamily: 'inherit',
    }
  });
export default styles;
