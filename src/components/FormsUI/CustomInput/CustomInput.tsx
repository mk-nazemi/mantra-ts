import React from "react";
import { FieldAttributes, useField } from "formik";
import sizes from "../../../styled/sizes";

interface AppProps {
  ourLabel: string;
}

type FormProps = FieldAttributes<{}> & AppProps;

const styles = {
  root: {
    display: "flex",
    justifyContent: "space-between",
    // alignItems: "center",
    height: "4rem",
    minWidth: " 35rem",
    width: "31rem",
    margin: "1rem 5rem 1rem 1rem"
  },
  label: {
    fontSize: "1.4rem",
    marginTop: "0.6rem"
  },
  input: {
    marginRight: "0.8rem",
    padding: "1rem",
    borderRadius: "3px",
    border: "none",
    "-webkit-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
    "-moz-box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)",
    "box-shadow": "-1px 4px 5px -1px rgba(168,168,168,0.28)"
  }
};

const _CustomInput: React.FC<FormProps> = formProps => {
  // console.log(formProps);

  const [field, meta] = useField<{}>(formProps);
  const { ourLabel, type, name } = formProps;
  return (
    <div style={styles.root}>
      <label style={styles.label} htmlFor={name}>
        {ourLabel}
      </label>
      <input style={styles.input} name={name} autoComplete="off" type={type} />
      {/* {this.renderError(meta)} */}
    </div>
  );
};

export const CustomInput = _CustomInput;
