import { createStyles } from "@material-ui/core/styles";
import sizes from "../../../styled/sizes";

const styles = () =>
  createStyles({
    container: {
      width: "100%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      padding: "0.5rem"
    },
    tabs: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      minHeight: "3rem",
      marginRight: "2rem"
    },
    tab: {
      cursor: "pointer",
      fontFamily: "Ganjnameh",
      fontSize: "1.4rem",
      padding: "0 0.5rem",
      minWidth: "200px",
      maxHeight: "3rem",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#faf5a5",
      marginLeft: "3rem",
      borderTopRightRadius: "4px",
      borderTopLeftRadius: "4px",
      [sizes.down("md")]: {
        fontSize: "1rem",
        width: "75%"
      },
      "&:hover": {
        background: "white"
      }
    },
    mainContent: {
      width: "100%",
      height: "100%",
      // border: "1px solid #faf5a5",
      border: "none",
      backgroundColor: "#fff",
      overflowY: "scroll",
      "-ms-overflow-style": "none",
      "&::-webkit-scrollbar": {
        display: "none"
      }
    }
  });
export default styles;
