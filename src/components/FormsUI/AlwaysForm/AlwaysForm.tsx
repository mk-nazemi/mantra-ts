import React, { useState, Dispatch, SetStateAction } from "react";
import styles from "./AlwaysFormStyle";
import { withStyles } from "@material-ui/styles";
import { ActiveTab } from "../CrewForm/CrewForm";

export enum Tabs {
  labelInfo = "جدول‬ ‫اطلاعات",
  labelEdit = " ثبت‬ ‫و‬ ویرایش‬"
}

interface AppProps {
  fn: Dispatch<SetStateAction<ActiveTab>>;
  children?: JSX.Element | JSX.Element[] | string;
  classes: {
    container: string;
    tabs: string;
    tab: string;
    mainContent: string;
  };
}

function _AlwaysForm(props: AppProps) {
  const { classes } = props;
  const setActive = (str: ActiveTab) => {
    props.fn(str);
  };
  return (
    <div className={classes.container}>
      <div className={classes.tabs}>
        <div
          role="presentation"
          onClick={() => setActive(ActiveTab.InformationTable)}
          className={classes.tab}
        >
          {Tabs.labelInfo}
        </div>
        <div
          role="presentation"
          onClick={() => setActive(ActiveTab.EditAndCommit)}
          className={classes.tab}
        >
          {Tabs.labelEdit}
        </div>
      </div>
      <div className={classes.mainContent}>{props.children}</div>
    </div>
  );
}

export const AlwaysForm = withStyles(styles)(_AlwaysForm);
