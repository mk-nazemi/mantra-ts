import React from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./ItemRoleStyles";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import blueGrey from "@material-ui/core/colors/blueGrey";
interface AppProps {
  classes: {
    root: string;
    settingIcon: string;
  };
}
const _ItemOfRole: React.FC<AppProps> = props => {
  const { classes } = props;

  const deleteItem = () => {
    // do delete
  };

  return (
    <div className={classes.root}>
      <div
        className={classes.settingIcon}
        role="presentation"
        onClick={deleteItem}
      >
        <span>جوجه معاون</span>
        <DeleteOutlineIcon color="disabled" fontSize="large" />
      </div>
    </div>
  );
};

export const ItemOfRole = withStyles(styles)(_ItemOfRole);
