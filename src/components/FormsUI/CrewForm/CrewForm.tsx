import React, { useState } from "react";
import { InformationTable } from "../InformationTable/InformationTable";
import { EditAndCommit } from "../EditAndCommit/EditAndCommit";
import { AlwaysForm } from "../AlwaysForm/AlwaysForm";

export enum ActiveTab {
  EditAndCommit = "EditAndCommit",
  InformationTable = "InformationTable"
}

function _CrewForm() {
  const [activeTab, setActiveTab] = useState(ActiveTab.EditAndCommit);
  return (
    <AlwaysForm fn={setActiveTab}>
      {activeTab === ActiveTab.InformationTable ? (
        <InformationTable />
      ) : (
        <EditAndCommit />
      )}
    </AlwaysForm>
  );
}

export const CrewForm = _CrewForm;
