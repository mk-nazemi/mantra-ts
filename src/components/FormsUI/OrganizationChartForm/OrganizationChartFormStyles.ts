import { createStyles } from "@material-ui/core/styles";
import sizes from "../../../styled/sizes";
import {colors} from "../../../styled/Variables";

const styles = () =>
	createStyles({
		root: {
			width: '100%',
			height: '100%',
			display: 'flex',
			backgroundColor: '#fff',
			padding: '2rem',
			fontFamily: 'Ganjnameh',
		},
		btnContainer: {
			display: "flex",
			justifyContent: "space-evenly",
			alignItems: "center",
			margin: "1rem auto",
			width: "80%",
			height: "5vh",
			[sizes.down("md")]: {
				width: "100%",
			},
		},
		treeSection: {
			width: '50%',
			marginLeft: '0.5rem',
			padding: '1rem',
		},
		listWrapper: {
			width: '100%',
			height: '100%',
			marginTop: '3rem',
			backgroundColor: '#fafafa',
			padding: '2rem',
		},
		searchContainer: {
			width: '100%',
			display: 'flex',
			justifyContent: 'center',
			padding: '0.5rem',
			position: 'relative',
		},
		search: {
			width: '60%',
			border: '2px solid #e8e8e8',
			borderRadius: '6px',
			minHeight: '3rem',
			outline: 'none',
			fontFamily: 'inherit',
		},
		searchIcon: {
			width: '3rem',
			height: '3rem',
			position: 'absolute',
			top: '0.9rem',
			left: '20%',
		},
		treeList: {
			minWidth: "85%",
			padding: 0,
			margin: "0.7rem auto 0",
			position: "relative",
			right: 0,
			transition: "right 0.2s cubic-bezier(0.820, 0.085, 0.395, 0.895)"
		},
		close: {
			right: "-28rem"
		},
		expansionPanelTitle: {
			fontFamily: "inherit",
			fontSize: "1.4rem",
			fontWeight: 'bold',
		},
		expansionPanelItemContainer: {
			backgroundColor: "#fff",
			padding: "1rem 0 1rem"
		},
		expansionPanelItem: {
			fontFamily: 'inherit',
			fontSize: "1.2rem",
			fontWeight: 'bold',
			borderRadius: "2px",
			padding: "0.5rem 0.8rem 0.5rem 0.5rem",
			width: "80%",
			margin: "0 auto",
			color: '#000',
			cursor: "pointer",
			transition: "0.2s",
			"&:last-child": {
				border: "none"
			},
			"&:hover": {
				backgroundColor: "#909090",
				color: "#000"
			}
		},
		editSection: {
			width: '50%',
			marginRight: '0.5rem',
			padding: '1rem',
		},
		editWrapper: {
			width: '100%',
			height: '100%',
			marginTop: '3rem',
			padding: '2rem',
			backgroundColor: '#fafafa',
		},
		inputWrapper: {
			width: '50%',
			margin: '0 auto',
			display: 'flex',
			justifyContent: 'space-between',
			marginBottom: '1rem',
		},
		inputText: {
			fontSize: '1.4rem',
			fontWeight: 'bold',
		},
		input: {
			outline: 'none',
			border: '2px solid #e8e8e8',
			borderRadius: '6px',
			minHeight: '3rem',
			fontFamily: 'inherit',
		},
		roleListTable: {
			width: '50%',
			margin: '3rem auto 0',
			height: '100%',
			display: 'flex',
		},
		roleListTitle: {
			width: '50%',
			fontSize: '1.4rem',
			fontWeight: 'bold',
			fontFamily: 'inherit',
		},
		roleContainer: {
			width: '50%',
			height: '100%',
			display: 'flex',
			flexDirection: 'column',
			fontFamily: 'inherit',
		},
		role: {
			width: '100%',
			minHeight: '3rem',
			backgroundColor: '#fff',
			marginBottom: '1rem',
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
			border: '2px solid #e8e8e8',
			padding: '1rem',
		},
		roleTitle: {
			fontSize: '1.4rem',
			fontWeight: 'bold',
		},
	});

export default styles;
