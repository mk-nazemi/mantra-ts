import React from "react";
import styles from './OrganizationChartFormStyles';
import { withStyles } from "@material-ui/styles";
import {ProceedButton} from "../../BUTTONS/ProceedButton/ProceedButton";
import add from "../../../assets/add.svg";
import {cancelProps, commitProps, deleteProps, newProps} from "../EditAndCommit/dummyData";
import floppy from "../../../assets/floppy-disk.svg";
import deleteLogo from "../../../assets/rubbish.svg";
import cancelLogo from "../../../assets/backspace.svg";
import Search from "../../icons/SearchIcon/SearchIcon";
import classNames from "classnames";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Delete from "../../icons/DeleteIcon/DeleteIcon";

interface AppProps {
	classes: {
		root: string;
		btnContainer: string;
		treeSection: string;
		listWrapper: string;
		searchContainer: string;
		search: string;
		searchIcon: string;
		treeList: string;
		expansionPanelTitle: string;
		expansionPanelItemContainer: string;
		expansionPanelItem: string;
		editSection: string;
		editWrapper: string;
		inputWrapper: string;
		inputText: string;
		input: string;
		roleListTable: string;
		roleListTitle: string;
		roleContainer: string;
		role: string;
		roleTitle: string;
	}
}

const ExpansionPanel = withStyles({
	root: {
		marginBottom: "0.8rem",
		border: "none",
		boxShadow: "none",

		"&:before": {
			display: "none"
		},
		"&$expanded": {
			margin: "auto"
		}
	},
	expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
	root: {
		padding: 0,
		paddingRight: "10px",
		background: "#fff",
		border: "none",
		minHeight: 40,
		color: "#000",
		"&$expanded": {
			minHeight: 40
		}
	},
	content: {
		"&$expanded": {
			margin: "12px 0"
		}
	},
	expandIcon: {
		color: "#fff"
	},
	expanded: {}
})(MuiExpansionPanelSummary);

// const Chart = [
// 	{
// 		role: 'مدیریت عامل',
// 		subset: [
// 			{
// 				role: 'مدیر منابع انسانی',
// 				subset:[]
// 			},
// 			{
// 				role: 'مدیر فروش',
// 				subset:[
// 					{
// 						role: 'معاون منطقه اي',
// 						subset: []
// 					},
// 					{
// 						role: 'بازاریاب',
// 						subset: []
// 					}
// 				]
// 			},
// 			{
// 				role: 'مدیر پشتیبانی',
// 				subset:[]
// 			},
// 			{
// 				role: 'مدیر اجرایی',
// 				subset:[]
// 			}
// 		]
// 	}
// ];
const treeData = ["اطلاعات پایه", "حسابداری", "فروش", "انبار"];

const subMenu = ["پرسنل", "چارت سازمانی", "نقش"];

const inputLabels = ['واحد سازمانی والد', 'منطقه', 'کد', 'عنوان', 'نقش'];

const roleListTable = ['مدیریت لیست ها', 'جمع آوري اسناد', '...'];

function _OrganizationChartForm(props: AppProps) {

	const { classes } = props;
	// const renderTreeSection = (data: any) => {
	// 	let children: Array<JSX.Element> = [];
	// 	if (data[index].subset === []) {
	// 		return;
	// 	}
	// 	if (data[index].subset !== []) {
	// 		data[index].subset.map((item: any) => {
	// 			children.push(
	// 				<ExpansionPanel>
	// 					<ExpansionPanelSummary
	// 						expandIcon={<ExpandMoreIcon/>}
	// 						aria-controls="panel1a-content"
	// 						id="panel1a-header"
	// 					>
	// 						<Typography className={classes.expansionPanelTitle}>
	// 							{item.role}
	// 						</Typography>
	// 					</ExpansionPanelSummary>
	// 				</ExpansionPanel>);
	// 			index = 1;
	// 			renderTreeSection(item);
	// 		});
	// 	}
	// 	return <ul className={classes.list}>{children}</ul>;
	// };

	return (
		<div className={classes.root}>
			<div className={classes.treeSection}>
				<div className={classes.btnContainer}>
					<ProceedButton iconName={add} btnProps={newProps} />
					<ProceedButton iconName={deleteLogo} btnProps={deleteProps} />
				</div>
				<div className={classes.listWrapper}>
					<div className={classes.searchContainer}>
						<input type="text" className={classes.search}/>
						<div className={classes.searchIcon}>
							<Search />
						</div>
					</div>
					<ul className={classes.treeList}>
						{treeData.map(item => (
							<ExpansionPanel key={item}>
								<ExpansionPanelSummary
									expandIcon={<ExpandMoreIcon />}
									aria-controls="panel1a-content"
									id="panel1a-header"
								>
									<Typography className={classes.expansionPanelTitle}>
										{item}
									</Typography>
								</ExpansionPanelSummary>
								{
									<div className={classes.expansionPanelItemContainer}>
										{subMenu.map(item => (
											<div
												key={item}
												role="presentation"
												className={classes.expansionPanelItem}
											>
												{item}
											</div>
										))}
									</div>
								}
							</ExpansionPanel>
						))}
					</ul>
				</div>
			</div>
			<div className={classes.editSection}>
				<div className={classes.btnContainer}>
					<ProceedButton iconName={floppy} btnProps={commitProps} />
					<ProceedButton iconName={cancelLogo} btnProps={cancelProps} />
				</div>
				<div className={classes.editWrapper}>
					{
						inputLabels.map((item) => (
							<div key={item} className={classes.inputWrapper}>
								<p className={classes.inputText}>{item}</p>
								<input className={classes.input} type="text" />
							</div>
						))
					}
					<div className={classes.roleListTable}>
						<p className={classes.roleListTitle}>لیست نقش</p>
						<div className={classes.roleContainer}>
							{
								roleListTable.map((item) => (
									<div key={item} className={classes.role}>
										<p className={classes.roleTitle}>{item}</p>
										<Delete color="black" />
									</div>
								))
							}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export const OrganizationChartForm = withStyles(styles)(_OrganizationChartForm);