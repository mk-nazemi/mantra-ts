import React from "react";
import styles from "./InformationTableStyles";
import { withStyles } from "@material-ui/styles";
import { ProceedButton } from "../../BUTTONS/ProceedButton/ProceedButton";
import { deleteProps } from "../EditAndCommit/dummyData";
import deleteLogo from "../../../assets/rubbish.svg";
interface AppProps {
  classes: {
    wrapper: string;
    informationTableProceedButtons: string;
  };
}

class _InformationTable extends React.Component<AppProps> {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        <div className={classes.informationTableProceedButtons}>
          <ProceedButton iconName={deleteLogo} btnProps={deleteProps} />
        </div>
      </div>
    );
  }
}

export const InformationTable = withStyles(styles)(_InformationTable);
