import { createStyles } from "@material-ui/core/styles";
import sizes from "../../../styled/sizes";

const styles = () =>
  createStyles({
    wrapper: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center"
    },
    informationTableProceedButtons: {
      display: "flex",
      width: "90%",
      margin: "0 auto",
      height: "3.5rem",
      marginTop: "2rem",
      marginBottom: "1rem"
    }
  });
export default styles;
