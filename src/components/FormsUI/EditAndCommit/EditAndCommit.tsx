import React from "react";
import styles from "./EditAndCommitStyles";
import { withStyles } from "@material-ui/styles";
import { ProceedButton } from "../../BUTTONS/ProceedButton/ProceedButton";
import cancelLogo from "../../../assets/backspace.svg";
import deleteLogo from "../../../assets/rubbish.svg";
import floppy from "../../../assets/floppy-disk.svg";
import add from "../../../assets/add.svg";
import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray,
  withFormik,
  FormikProps
} from "formik";
import * as yup from "yup";

import {
  personInfoData,
  accountData,
  rolesData,
  cancelProps,
  deleteProps,
  commitProps,
  newProps
} from "./dummyData";
import { CustomInput } from "../CustomInput/CustomInput";
import ToggleSwitch from "../../BUTTONS/ToggleSwitch/ToggleSwitch";
import { Introduction } from "../../layout/Introduction/Introduction";
import { InvokerButton } from "../../BUTTONS/InvokerButton/InvokerButton";
import { ItemOfRole } from "../ItemRole/ItemOfRole";

enum labels {
  firstName = "نام",
  lastName = "نام خانوادگی",
  nationalCode = "کد ملی",
  birthDate = " تاریخ تولد",
  personnelCode = "کد پرسنل",
  userName = "نام کاربری",
  password = "رمز",
  rePassword = "تکرار رمز",
  expireDate = "تاریخ انقضا",
  isActive = "فعال"
}

interface FormValues {
  firstName?: string;
  lastName?: string;
  nationalCode?: string;
  birthDate?: Date;
  personnelCode?: number;
  userName?: string;
  password?: string;
  rePassword?: string;
  expireDate?: Date;
  isActive?: boolean;
  personnelPosition?: string;
}

interface AppProps {
  classes: {
    root: string;
    form: string;
    btnContainer: string;
    personContainer: string;
    persons: string;
    roleContainer: string;
    position: string;
    userInfo: string;
    positionHeader: string;
    positionForm: string;
    userInfoHeader: string;
    userInfoForm: string;
    switch: string;
  };
}

const validationSchema = yup.object({
  firstName: yup
    .string()
    .required()
    .max(10),
  lastName: yup.string(),
  isActive: yup.boolean(),
  yugort: yup.string(),
  pets: yup.array().of(
    yup.object({
      name: yup.string().required()
    })
  )
});
function __EditAndCommit(props: AppProps & FormikProps<FormValues>) {
  const { classes } = props;

  const initValues: FormValues = {
    firstName: "",
    lastName: "",
    nationalCode: "",
    birthDate: new Date(),
    personnelCode: 0,
    userName: "",
    password: "",
    rePassword: "",
    expireDate: new Date(),
    isActive: false,
    personnelPosition: ""
  };

  return (
    <div className={classes.root}>
      {/* <Formik
        initialValues={initValues}
        validationSchema={validationSchema}
        // come from formik bag
        onSubmit={(data, { setSubmitting, resetForm, setErrors }) => {
          setSubmitting(true);
          // async call here
          console.log("submit form :", data);
          // async call finish
          setSubmitting(false);
          resetForm();
        }}
      > */}
      <Form className={classes.form}>
        <div className={classes.btnContainer}>
          <ProceedButton iconName={add} btnProps={newProps} />
          <ProceedButton iconName={floppy} btnProps={commitProps} />
          <ProceedButton iconName={deleteLogo} btnProps={deleteProps} />
          <ProceedButton iconName={cancelLogo} btnProps={cancelProps} />
        </div>
        <div className={classes.personContainer}>
          <div className={classes.persons}>
            {/* <Field name="firstName" type="input" as={CustomInput} /> */}

            <CustomInput
              name="firstName"
              type="input"
              ourLabel={labels.firstName}
            />
            <CustomInput
              name="lastName"
              type="input"
              ourLabel={labels.lastName}
            />
            <CustomInput
              name="nationalCode"
              type="input"
              ourLabel={labels.nationalCode}
            />
            <CustomInput
              name="birthDate"
              type="input"
              ourLabel={labels.birthDate}
            />
            <CustomInput
              name="personnelCode"
              type="input"
              ourLabel={labels.personnelCode}
            />
          </div>
        </div>
        <div className={classes.roleContainer}>
          <div className={classes.position}>
            <div className={classes.positionHeader}>
              <p>پست سازمانی</p>
            </div>
            <div className={classes.positionForm}>
              <InvokerButton />
              <ItemOfRole />
            </div>
          </div>
          <div className={classes.userInfo}>
            <div className={classes.userInfoHeader}>
              <p>مشخصات کاربری</p>
            </div>
            <div className={classes.userInfoForm}>
              <CustomInput
                name="userName"
                type="input"
                ourLabel={labels.userName}
              />
              <CustomInput
                name="password"
                type="input"
                ourLabel={labels.password}
              />
              <CustomInput
                name="rePassword"
                type="input"
                ourLabel={labels.rePassword}
              />
              <CustomInput
                name="expireDate"
                type="input"
                ourLabel={labels.expireDate}
              />
              <div className={classes.switch}>
                <label htmlFor="isActive" style={{ fontSize: "1.2rem" }}>
                  {labels.isActive}
                </label>
                <Field name="isActive" type="input" as={ToggleSwitch} />
              </div>
            </div>
          </div>
        </div>
      </Form>
      {/* </Formik> */}
      {/* <div className={classes.editAndCommitProceedButtons}>
        <ProceedButton iconName={add} btnProps={newProps} />
        <ProceedButton iconName={floppy} btnProps={commitProps} />
        <ProceedButton iconName={deleteLogo} btnProps={deleteProps} />
        <ProceedButton iconName={cancelLogo} btnProps={cancelProps} />
      </div>
      <div style={{ width: "100%", backgroundColor: "#E4EBF7" }}>
        <div className={classes.personInfoContainer}>
          {personInfoData.map(item => (
            <div className={classes.personInfo}>
              <p className={classes.personInfoText}>{item}:</p>
              <input className={classes.personInfoInput} type="text" />
            </div>
          ))}
        </div>
      </div> */}
      {/* <div className={classes.accountAndRoleContainer}> 
      <div className={classes.roleContainer}> *
      {<p className={classes.roleTitle}>‫پست‬ ‫سازمانی‬</p>
          <button className={classes.roleSearchButton}>
            ‫‬‫فرم‬ جستجو ‫پست ‬‫سازمانی‬
          </button>
          <div className={classes.rolesSection}>
            {rolesData.map(role => (
              <div className={classes.role}>
                <p>{role}</p>
                <Delete color="black" />
              </div>
            ))}
          </div>
        </div>
        <div className={classes.accountContainer}>
          <p className={classes.accountTitle}>مشخصات‬ ‫کاربري‬</p>‫
          <div className={classes.accountSection}>
            {accountData.map(item => (
              <div className={classes.accountDetail}>
                <p>{item}:</p>
                <input className={classes.accountDetailInput} type="text" />
              </div>
            ))}
          </div>
        </div>
      </div>  */}
    </div>
  );
}

interface MyFormProps {
  firstName?: string;
  lastName?: string;
  nationalCode?: string;
  birthDate?: Date;
  personnelCode?: number;
  userName?: string;
  password?: string;
  rePassword?: string;
  expireDate?: Date;
  isActive?: boolean;
  personnelPosition?: string;
}

const _EditAndCommit = withStyles(styles)(__EditAndCommit);
export const EditAndCommit = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => {
    return {
      username: "",
      password: "",
      key: ""
    };
  },
  // Transform outer props into form values
  // Add a custom validation function (this can be async too!)
  validate: (values: FormValues) => {
    // let errors: FormikErrors = {};
    // if (!values.email) {
    //   errors.email = "Required";
    // } else if (!isValidEmail(values.email)) {
    //   errors.email = "Invalid email address";
    // }
    // return errors;
  },
  handleSubmit(values: FormValues): void {
    // do submitting things
    console.log(values);
  }
})(_EditAndCommit);
