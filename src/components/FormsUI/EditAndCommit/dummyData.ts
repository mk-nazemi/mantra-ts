import { newColor } from "../../../styled/Variables";
export const personInfoData = [
  "‫نام‬",
  "‫نام ‫خانوادگی‬",
  "‫کد‬ ‫ملی‬",
  "‫‫تاریخ‬ تولد‬",
  "‫‫شماره‬ پرسنلی‬"
];

export const accountData = [
  "‫نام‬ ‫کاربري‬",
  "‫رمز‬ ‫عبور‬",
  "‫تکرار‬ ‫رمز‬",
  "‫تاریخ‬ ‫انقضا‬"
];

export const rolesData = ["‫‫مدیر‬ فناوري‬", "‫مدیر‬ ‫‫فروشگاه‬ ها‬"];
export const cancelProps = {
  backColor: "rgb(255, 134, 134)",
  textColor: "#fff",
  textOf: "انصراف",
  alt: "انصراف"
};
export const deleteProps = {
  backColor: "rgb(215, 29, 29)",
  textColor: "#fff",
  textOf: "حذف",
  alt: "حذف"
};
export const commitProps = {
  backColor: "#dff0ea",
  textColor: "#000",
  textOf: "ثبت",
  alt: "ثبت"
};

export const newProps = {
  backColor: "#12d3cf",
  textColor: "#fff",
  textOf: "جدید",
  alt: "جدید"
};
