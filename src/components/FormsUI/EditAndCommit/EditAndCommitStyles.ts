import { createStyles } from "@material-ui/core/styles";
import { newColor } from "../../../styled/Variables";
import sizes from "../../../styled/sizes";

const styles = () =>
  createStyles({
    root: {
      display: "flex",
      height: "100%",
      width: "100%",
      // background: "red",
      flexDirection: "column",
      alignItems: "center"
    },
    form: {
      width: '100%',
      padding: '0 2rem',
    },
    btnContainer: {
      display: "flex",
      justifyContent: "space-evenly",
      alignItems: "center",
      margin: "1rem auto",
      width: "80%",
      height: "5vh",
      [sizes.down("md")]: {
        width: "100%",
      },
    },
    personContainer: {
      width: "100%",
    },
    persons: {
      padding: "1rem",
      width: "100%",
      height: "60%",
      margin: "1rem auto 0rem",
      background: `${newColor.bg}`,
      // backgroundColor: "red",
      "@media (max-width: 600px)": {},
      // alignItems: "center",
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap"
    },
    roleContainer: {
      margin: "0.3rem auto 0rem",
      background: `${newColor.bg}`,
      height: "52vh",
      width: "100%",
      display: "flex",
      justifyContent: "space-evenly",
      alignItems: "center"
    },
    position: {
      height: "96%",
      background: "white",
      width: "25%"
    },
    userInfo: {
      marginRight: "0.3rem",
      height: "96%",
      background: "white",
      width: "72%"
    },
    positionHeader: {
      fontSize: "1.8rem",
      paddingRight: "3rem",
      paddingTop: "1rem"
    },
    positionForm: {
      height: "85%",
      marginTop: "1rem",
      background: `${newColor.bg}`,
      width: "90%",
      margin: "0 auto"
    },
    userInfoHeader: {
      fontSize: "1.8rem",
      paddingRight: "3rem",
      paddingTop: "1rem",
      marginBottom: "1rem"
    },
    userInfoForm: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      background: `${newColor.bg}`,
      height: "85%",
      width: "97%",
      margin: " 0 auto"
    },
    switch: {
      display: "flex",
      justifyContent: "space-evenly",
      alignItems: "center",
      marginLeft: "8.6rem",
      marginTop: "1rem",
      width: "46%"
    }
  });

export default styles;
