import { createStyles } from "@material-ui/core/styles";


const backColor = "#E4EBF7";

const styles = () =>
  createStyles({
    root: {
      display: "flex",
      width: "100%",
      // background: "red",
      flexDirection: "column",
      alignItems: "center"
    },
    roleContainer: {
      margin: "0.3rem auto 0rem",
      background: '#f3f3f3',
      height: "52vh",
      width: "100%",
      display: "flex",
      justifyContent: "space-evenly",
      alignItems: "center"
    },
    position: {
      height: "96%",
      background: "white",
      width: "25%"
    },
    userInfo: {
      marginRight: "0.3rem",
      height: "96%",
      background: "white",
      width: "72%"
    },
    positionHeader: {
      fontSize: "1.8rem",
      paddingRight: "3rem",
      paddingTop: "1rem"
    },
    positionForm: {
      height: "85%",
      marginTop: "1rem",
      background: '#f3f3f3',
      width: "90%",
      margin: "0 auto"
    },
    userInfoHeader: {
      fontSize: "1.8rem",
      paddingRight: "3rem",
      paddingTop: "1rem",
      marginBottom: "1rem"
    },
    userInfoForm: {
      background: '#f3f3f3',
      height: "85%",
      marginTop: "1rem",
      width: "97%",
      margin: "0 auto"
    }
  });
export default styles;
