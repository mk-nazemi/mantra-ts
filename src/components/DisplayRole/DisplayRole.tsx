import React from "react";
import styles from "./DisplayRoleStyle";
import { withStyles } from "@material-ui/styles";

interface AppProps {
  //ActiveLicense:boolean;
    classes: {
      root: string;
      roleContainer: string;
      position: string;
      userInfo: string;
      positionHeader: string;
      positionForm: string;
      userInfoHeader: string;
      userInfoForm: string;
    };
    HeaderLabel:string;
    ListLabel:string;
  }
  
function _DisplayRole(props: AppProps) {
    const { classes, HeaderLabel, ListLabel } = props;
    return (
      <div className={classes.root}>
        <div className={classes.roleContainer}>
          <div className={classes.position}>
            <div className={classes.positionHeader}>
              <p>{HeaderLabel}</p>
            </div>
            <div className={classes.positionForm}></div>
          </div>
          <div className={classes.userInfo}>
            <div className={classes.userInfoHeader}>
              <p>{ListLabel}</p>
            </div>
            <div className={classes.userInfoForm}></div>
          </div>
        </div>
      </div>
    );
  }
  
  export const DisplayRole = withStyles(styles)(_DisplayRole);
 
  