// styles
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";

// package
import React from "react";
import { AgGridReact } from "ag-grid-react";
// reuse components

class _TableMain extends React.Component {
  state = {
    columnDefs: [
      {
        headerName: "Make",
        field: "make",
        sortable: true,
        checkboxSelection: true
      },
      { headerName: "Model", field: "model", sortable: true },
      { headerName: "Price", field: "price", sortable: true },
      { headerName: "Account", field: "account", sortable: true },
      { headerName: "Position", field: "position", sortable: true },
      { headerName: "Location", field: "location", sortable: true },
      { headerName: "More", field: "more", sortable: true }
    ],
    rowData: [
      {
        make: "Toyota",
        model: "Celica",
        price: 35000,
        account: "current",
        position: "near there",
        location: "clean tehran",
        more: " ... "
      },
      {
        make: "Ford",
        model: "Mondeo",
        price: 32000,
        account: "current",
        position: "near there",
        location: "clean tehran",
        more: " ... "
      },
      {
        make: "Porsche",
        model: "Boxter",
        price: 72000,
        account: "current",
        position: "near there",
        location: "clean tehran",
        more: " ... "
      },
    ]
  };

  render() {
    return (
      <div
        className="ag-theme-balham"
        style={{ height: "50%", width: "100%", direction: "rtl" }}
      >
        <AgGridReact
          rowSelection="multiple"
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData}
        />
      </div>
    );
  }
}

export const TableMain = _TableMain;
