import React from 'react';
import { withStyles } from "@material-ui/styles";
import styles from "./ModalStyles";
import Backdrop from "@material-ui/core/Backdrop";
import Modal from "@material-ui/core/Modal";

interface AppProps {
  setIsOpen: Function
  isOpen: boolean
  classes: {
    modal: string
  }
  children: React.ReactElement
}

function _MuiModal(props: AppProps) {
  const { classes, children, setIsOpen, isOpen } = props;

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={isOpen}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      {children}
    </Modal>
  )
}

export const MuiModal = withStyles(styles)(_MuiModal);