import React from "react";
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { withStyles } from "@material-ui/styles";
import styles from "./SwitchLanguageButtonStyle";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        left:"6rem"
      },
    },
  }),
);

function _SwitchLanguageButton() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button variant="contained" color="primary" href="#contained-buttons">
       English
      </Button>
    </div>
  );
}
export const SwitchLanguageButton = withStyles(styles)(_SwitchLanguageButton);