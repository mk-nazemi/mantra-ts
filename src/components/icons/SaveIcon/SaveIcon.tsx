import React from 'react';
import { styled } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';

const Icon = styled(SaveIcon)({
  width: '3rem',
  height: '3rem',
});

const Save = () => {
  return <Icon />;
};

export default Save;
