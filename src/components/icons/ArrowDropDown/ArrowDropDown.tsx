import React from "react";
import { styled } from "@material-ui/core/styles";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

interface IconProps {
  color?:string,
}

const Icon = styled(ArrowDropDownIcon)({
  width: "3rem",
  height: "3rem",
  color: "#79589f",
  position:"absolute",
  top:"12px",
  left:"5px",
});

const ArrowDropDown = (props:IconProps) => {
  return <Icon  style ={{color:props.color}}/>;
};

export default ArrowDropDown;
