import React from "react";
import { styled } from "@material-ui/core/styles";
import GroupWorkIcon from '@material-ui/icons/GroupWork';

const Icon = styled(GroupWorkIcon)({
	width: "3rem",
	height: "3rem",
	color: "#79589f",
});

const GroupWork = () => {
	return <Icon />;
};

export default GroupWork;
