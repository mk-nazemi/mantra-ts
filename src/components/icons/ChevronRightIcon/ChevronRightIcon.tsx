import React from 'react';
import PropTypes from "prop-types";
import { styled } from '@material-ui/core/styles';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { colors, radius } from '../../../styled/Variables';

interface Icon {
  className: string
}

const Icon = styled(ChevronRightIcon)({
  width: '2rem',
  height: '2rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#dff0ea',
  borderRadius: radius.circle,
  color: colors.gray500,
});

const ChevronRight = (props: Icon) => {
  return <Icon className={props.className} />;
};

ChevronRight.propTypes = {
  className: PropTypes.string,
};

export default ChevronRight;
