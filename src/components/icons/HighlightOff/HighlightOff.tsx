import React from "react";
import { styled } from "@material-ui/core/styles";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const Icon = styled(HighlightOffIcon)({
  width: "1.6rem",
  height: "1.6rem",
  color: "#4c4c4c"
});

const HighlightOff = () => {
  return <Icon />;
};

export default HighlightOff;
