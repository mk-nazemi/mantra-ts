import React from 'react';
import { styled } from '@material-ui/core/styles';
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const Icon = styled(ExitToAppIcon)({
  width: '2.5rem',
  height: '2.5rem',
  color: '#fff',
});

const Save = () => {
  return <Icon />;
};

export default Save;
