import React from "react";
import { styled } from "@material-ui/core/styles";
// import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import PowerSettingsNewTwoToneIcon from "@material-ui/icons/PowerSettingsNewTwoTone";
interface Icon {
  className: string;
}

const Icon = styled(PowerSettingsNewTwoToneIcon)({
  width: "3.3rem",
  height: "3.3rem",
  display: "flex",
  textAlign: "center",
  margin: "0 auto"
});

const PowerSettings = (props: Icon) => {
  return <Icon className={props.className} />;
};

export default PowerSettings;
