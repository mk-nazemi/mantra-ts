import React from 'react';
import { styled } from '@material-ui/core/styles';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';

const Icon = styled(SearchOutlinedIcon)({
	width: '30px',
	height: '30px',
	color: '#c2c2c2',
});

const Search = () => {
	return <Icon />;
};

export default Search;
