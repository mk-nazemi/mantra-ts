import React from 'react';
import { styled } from '@material-ui/core/styles';
import AddBoxIcon from '@material-ui/icons/AddBox';

const Icon = styled(AddBoxIcon)({
  width: '30px',
  height: '30px',
  color: '#fff',
});

const AddBox = () => {
  return <Icon />;
};

export default AddBox;
