import React from 'react';
import { styled } from "@material-ui/core/styles";
import PersonIcon from '@material-ui/icons/Person';

const Icon = styled(PersonIcon)({
    width: "3rem",
    height: "3rem",
    color: "#79589f",
});
const Person = () => {
    return <Icon />
};
export default Person;