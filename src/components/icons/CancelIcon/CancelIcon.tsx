import React from 'react';
import { styled } from '@material-ui/core/styles';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';

const Icon = styled(CancelPresentationIcon)({
	width: '30px',
	height: '30px',
});

const Cancel = () => {
	return <Icon />;
};

export default Cancel;
