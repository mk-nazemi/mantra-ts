import React from 'react';
import { styled } from '@material-ui/core/styles';
import SettingsIcon from "@material-ui/icons/Settings";

const Icon = styled(SettingsIcon)({
  width: '2.5rem',
  height: '2.5rem',
  color: '#fff',
});

const Setting = () => {
  return <Icon />;
};

export default Setting;
