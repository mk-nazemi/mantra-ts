import React from "react";
import { styled } from "@material-ui/core/styles";
import HelpIcon from "@material-ui/icons/Help";

interface Icon {
  className: string;
}

const Icon = styled(HelpIcon)({
  width: "3.3rem",
  height: "3.3rem",
  display: "flex",
  margin: "0 auto"
});

const Help = (props: Icon) => {
  return <Icon className={props.className} />;
};

export default Help;
