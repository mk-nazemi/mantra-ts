import React from 'react';
import { styled } from '@material-ui/core/styles';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { colors, radius } from '../../../styled/Variables';

interface Icon {
  className: string
}

const Icon = styled(ChevronLeftIcon)({
  width: '2rem',
  height: '2rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#dff0ea',
  borderRadius: radius.circle,
  color: colors.gray500,
});

const ChevronLeft = (props: Icon) => {
  return <Icon className={props.className} />;
};

export default ChevronLeft;
