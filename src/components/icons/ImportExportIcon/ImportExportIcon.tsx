import React from 'react';
import { styled } from '@material-ui/core/styles';
import ImportExportIcon from '@material-ui/icons/ImportExport';

const Icon = styled(ImportExportIcon)({
  width: '30px',
  height: '30px',
});

const ImportExport = () => {
  return <Icon />;
};

export default ImportExport;
