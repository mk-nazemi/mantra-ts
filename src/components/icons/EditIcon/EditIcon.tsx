import React from 'react';
import { styled } from '@material-ui/core/styles';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';

const Icon = styled(EditOutlinedIcon)({
	width: '30px',
	height: '30px',
});

const Edit = () => {
	return <Icon />;
};

export default Edit;
