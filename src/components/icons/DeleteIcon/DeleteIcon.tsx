import React from 'react';
import { styled } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';

interface IconProps {
	color?: string,
}

const Icon = styled(DeleteIcon)({
	width: '30px',
	height: '30px',
	color: '#fff'
});

const Delete = (props: IconProps) => {
	return <Icon style={{color: props.color}} />;
};

export default Delete;
