import React from 'react';
import { styled } from "@material-ui/core/styles";
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';

const Icon = styled(LockOpenOutlinedIcon)({
    width: "3rem",
    height: "3rem",
    color: "#79589f",
    transform: "rotate(0deg)",
    transformOrigin: "100% 50%",
});
const Lock = () => {
    return <Icon />
};
export default Lock;
