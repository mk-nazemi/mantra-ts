import React, { useState } from "react";
import PropTypes from "prop-types";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import DatePicker from "react-modern-calendar-datepicker";

const _SingleDatePicker = props => {
  const { setDate, date } = props;
  const [selectedDay, setSelectedDay] = useState(date);
  const handleDateChange = date => {
    setDate(date);
    setSelectedDay(date);
  };

  return (
    <div style={{ background: "red" }}>
      <DatePicker
        value={selectedDay}
        onChange={handleDateChange}
        shouldHighlightWeekends
        locale="fa"
      />
    </div>
  );
};

_SingleDatePicker.propTypes = {
  setDate: PropTypes.func,
  date: PropTypes.shape({
    year: PropTypes.number.isRequired,
    month: PropTypes.number.isRequired,
    day: PropTypes.number.isRequired
  })
};

export const SingleDatePicker = _SingleDatePicker;
