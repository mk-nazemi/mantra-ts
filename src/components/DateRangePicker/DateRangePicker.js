import React, { useState } from "react";
import PropTypes from 'prop-types';
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import DatePicker from "react-modern-calendar-datepicker";

const _DateRangePicker = (props) => {
  const [selectedDayRange, setSelectedDayRange] = useState({
    from: props.fromDate,
    to: props.toDate
  });
  return (
    <DatePicker
      value={selectedDayRange}
      onChange={setSelectedDayRange}
      inputPlaceholder="Select a day range"
      shouldHighlightWeekends
      locale="fa"
    />
  );
};

_DateRangePicker.propTypes = {
  fromDate: PropTypes.shape({
    year: PropTypes.number.isRequired,
    month: PropTypes.number.isRequired,
    day: PropTypes.number.isRequired,
  }),
  toDate: PropTypes.shape({
    year: PropTypes.number.isRequired,
    month: PropTypes.number.isRequired,
    day: PropTypes.number.isRequired,
  }),
};

export const DateRangePicker = _DateRangePicker;
