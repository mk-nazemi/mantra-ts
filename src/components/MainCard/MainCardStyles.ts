import { createStyles, Theme } from "@material-ui/core/styles";
import { newColor } from "../../styled/Variables";
const styles = (theme: Theme) =>
  createStyles({
    card: {
      minWidth: 275,
      width: "100%",
      height: "100%",
      marginLeft: "auto",
      background: "none",
      border: `1px solid #333366`,
      boxShadow: '0px 13px 49px -10px rgba(0,0,0,0.7)',
      borderTopRightRadius: 0,
    },
    cardFullSize: {
      width: "100%"
    }
  });

export default styles;
