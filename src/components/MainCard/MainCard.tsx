// styles
import styles from "./MainCardStyles";
// packages
import React from "react";
import { withStyles } from "@material-ui/styles";
import classNames from "classnames";
// re use components
import Card from "@material-ui/core/Card";
//
interface AppProps {
  children?: JSX.Element | JSX.Element[] | string;
  isFullSize: boolean;
  classes: {
    card: string;
    cardFullSize: string;
  };
}

function _MainCard(props: AppProps) {
  const { classes, isFullSize } = props;
  return (
    <Card
      className={classNames(classes.card, {
        [classes.cardFullSize]: isFullSize
      })}
    >
      {props.children}
    </Card>
  );
}

export const MainCard = withStyles(styles)(_MainCard);
